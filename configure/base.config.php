<?php
/*
 * Archivo de configuraci�n de las clases que usaremos
 * Se llama desde Configure::getClass('NombreClase');
 */

/**
 * Engine:  Aquests par�metres no els toqueu.
 */
//$config['factory']						=  PATH_ENGINE . 'factory.class.php';
//$config['sql']							=  PATH_ENGINE . 'sql.class.php';


$config['mail']							=  PATH_ENGINE . 'mail.class.php';
$config['session']						=  PATH_ENGINE . 'session.class.php';
$config['user']							=  PATH_ENGINE . 'user.class.php';
$config['url']							=  PATH_ENGINE . 'url.class.php';
$config['uploader']						=  PATH_ENGINE . 'uploader.class.php';


$config['dispatcher']					=  PATH_ENGINE . 'dispatcher.class.php';


/** 
 * Controllers i Models 
 *
 * Aqui cal que cada nou controller i model que feu servir quedi declarat. 
 * Si no ho feu, no funcionar� qual el crideu des d'una ruta definida al fitxer dispatcher.config.php
 */

// Controller per 404
$config['ErrorError404Controller']		= PATH_CONTROLLERS . 'error/error404.ctrl.php';

// Controller Home
$config['HomeHomeController']			= PATH_CONTROLLERS . 'home/home.ctrl.php';

// Controller Register i Login
$config['LoginRegisterController']      = PATH_CONTROLLERS . 'login/register.ctrl.php';
$config['LoginLoginController']         = PATH_CONTROLLERS . 'login/login.ctrl.php';

//Controllers Reviews
$config['ReviewNewController']          = PATH_CONTROLLERS . 'review/new.ctrl.php';
$config['ReviewReviewController']       = PATH_CONTROLLERS . 'review/review.ctrl.php';
$config['ReviewMyReviewsController']    = PATH_CONTROLLERS . 'review/myreviews.ctrl.php';
$config['ReviewMyVotesController']      = PATH_CONTROLLERS . 'review/myvotes.ctrl.php';


// Controller Search
$config['SharedSearchController']       = PATH_CONTROLLERS . 'shared/search.ctrl.php';
$config['SharedPointController']        = PATH_CONTROLLERS . 'shared/point.ctrl.php';
$config['SharedBestReviewsController']   = PATH_CONTROLLERS . 'shared/bestreviews.ctrl.php';




// Model User
$config['UserUserModel']                = PATH_MODELS . 'user/user.model.php';

// Model Reviews
$config['ReviewReviewModel']            = PATH_MODELS . 'review/review.model.php';


// Controllers compartits per tota la p�gina
$config['SharedHeadController']			= PATH_CONTROLLERS . 'shared/head.ctrl.php';
$config['SharedFooterController']		= PATH_CONTROLLERS . 'shared/footer.ctrl.php';