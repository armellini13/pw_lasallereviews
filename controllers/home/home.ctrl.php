<?php
/**
 * Home Controller: Home de la pàgina.
 */
class HomeHomeController extends Controller
{
	protected $view = 'home/home.tpl';

    public function build()
    {
        $error = false;

        $info = $this->getParams();

        if(isset($info["url_arguments"])){
            if(!(sizeof($info["url_arguments"])==1 && $info["url_arguments"][0]==""))
                $error = true;
        }

        if($error)
            $this->setLayout('error/error404.tpl');
        else {

            $reviewsModel = $this->getClass('ReviewReviewModel');
            $bestReview = $reviewsModel->getBestReview();
            $lastTen = $reviewsModel->getLastTenReviews();
            $lastPhotos = $reviewsModel->getLastTenPhotos();

            $this->assign('bestReview',$bestReview);
            $this->assign('lastTen',$lastTen);
            $this->assign('lastPhotos',$lastPhotos);
            $this->setLayout($this->view);
        }
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
        $modules['bestreviews']	= 'SharedBestReviewsController';
        return $modules;
	}
}