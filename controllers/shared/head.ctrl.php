<?php



class SharedHeadController extends Controller
{
	const REVISION = 31;

	public function build( )
	{
		$this->assign( 'revision', self::REVISION );

        $info = $this->getParams();

        $logged = false;
        $user = Session::getInstance()->get('user');

        if(isset($user))
            $logged = true;

        $this->assign('logged',$logged);
        $this->assign('user',$user);

        if(isset($info['key_main_controller']))
            $this->assign('header',$info['key_main_controller']);

        $this->setLayout( 'shared/head.tpl' );
	}
}


?>
