<?php
/**
 * Review Controller.
 */
class SharedPointController extends Controller
{
    protected $view = 'shared/point.tpl';

    public function build()
    {
        $error = false;

        $info = $this->getParams();

        if(isset($info['review'])){
            $review = $info['review'];
            $this->assign('review',true);
            $this->assign('reviewInfo',$info['review']);


            $user = Session::getInstance()->get('user');

            if($user){

                $reviewModel = $this->getClass('ReviewReviewModel');
                $myPoint = $reviewModel->getMyPoint($info['review']['ID'],$user);
                $this->assign('myPoint',$myPoint);
                $this->pointReview($info['review']['ID'],$user);
                $this->assign('registered',true);

            }
        }

        $this->setLayout($this->view);
    }


    private function pointReview($review,$user) {

        $point = Filter::getInteger('pointReview');

        if($point){

            $reviewModel = $this->getClass('ReviewReviewModel');
            $reviewModel->pointReview($review,$point,$user);

        }
    }
}

