<?php
/**
 * Review Controller.
 */
class SharedBestReviewsController extends Controller
{
    protected $view = 'shared/bestreviews.tpl';

    public function build()
    {
        $error = false;

        $reviewModel = $this->getClass('ReviewReviewModel');
        $bestReviews = $reviewModel->getBestReviews();

        $this->assign('bestReviews',$bestReviews);
        $this->setLayout($this->view);

    }

}