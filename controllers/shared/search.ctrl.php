<?php
/**
 * Search Controller: Permet buscar una review.
 */

class SharedSearchController extends Controller
{
    protected $view = 'shared/search.tpl';

    public function build()
    {
        $error = false;

        $info = $this->getParams();

        if(isset($info["url_arguments"])){
            if(!(sizeof($info["url_arguments"])==1 && $info["url_arguments"][0]==""))
                $error = true;
        }

        if($error)
            $this->setLayout('error/error404.tpl');
        else {

            $match = $this->search($results);
            if($match) {
                $this->assign('results',$results);
                $this->getPage(count($results));
                $this->assign('match',count($results));
                $this->assign('limit',count($results)-10);
            }
            else
                $this->assign('no',true);


            $this->setLayout($this->view);

        }
    }

    private function getPage($max) {

        $previous = Filter::getInteger('previous');
        $next = Filter::getInteger('next');
        $side = Filter::getString('side');

        $this->assign('future',$next+10);


        if($side == "next") {
            $this->assign('previous',$previous+10);
            if($next >= $max)
                $this->assign('next',0);
            else
                $this->assign('next',$next+10);
        }
        else{
            if($side == "previous") {
                $this->assign('previous',$previous-10);
                $this->assign('next',$next-10);
            }
            else{
                $this->assign('previous',0);
                $this->assign('next',10);

            }
        }

    }

    private function search(&$results) {

        $search = Filter::getString('search');

        if($search) {
            $this->assign('search',$search);
            $reviewModel = $this->getClass('ReviewReviewModel');
            $results = $reviewModel->search($search);
            return true;
        }
        else
            return false;
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        $modules['bestreviews']	= 'SharedBestReviewsController';
        return $modules;
    }
}