<?php
/**
 * Review Controller.
 */
class ReviewReviewController extends Controller
{
    protected $view = 'review/review.tpl';

    public function build()
    {
        $error = false;
        $page = 0;
        $review = "";
        $totalPages = 0;

        $info = $this->getParams();

        if(isset($info["url_arguments"])) {

            if(sizeof($info["url_arguments"])==1) {

                $reviewModel = $this->getClass('ReviewReviewModel');

                if($info["url_arguments"][0]!="") {

                    if(is_numeric($info["url_arguments"][0])) {
                        $totalPages = ceil($reviewModel->getReviewSize() / 10);

                        if($info["url_arguments"][0] >= 0 && $info["url_arguments"][0] < $totalPages) {
                            $page = $info["url_arguments"][0];
                        }
                        else
                            $error = true;
                    }
                    else {
                        $review = $reviewModel->getReview($info["url_arguments"][0]);
                        if(!$review) {
                            $review = $reviewModel->getOldReview($info["url_arguments"][0]);
                            if($review) {
                                header('Location: http://gagus.local/r/'.$review);
                            }
                            else
                                $error = true;
                        }
                        else
                            $this->setParams(array('review'=> $review));
                    }
                }
                else
                    $totalPages = ceil($reviewModel->getReviewSize() / 10);
            }
            else
                $error = true;
        }
        else {
            $reviewModel = $this->getClass('ReviewReviewModel');
            $totalPages = ceil($reviewModel->getReviewSize() / 10);
        }

        if($error)
            $this->setLayout('error/error404.tpl');
        else {

            if(!$review) {
                $reviewArray = $reviewModel->getReviewsFrom($page);
                $this->assign('reviewArray',$reviewArray);
                $this->assign('totalPages',$totalPages);
                $this->assign('page',$page+1);
                $this->assign('previous',$page-1);
            }

            $this->assign('review',$review);
            $this->setLayout($this->view);
        }


    }


    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        $modules['point']   = 'SharedPointController';
        $modules['bestreviews']	= 'SharedBestReviewsController';
        return $modules;
    }
}