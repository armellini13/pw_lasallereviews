<?php
/**
 * Home Controller: Home de la pàgina.
 */
class ReviewMyVotesController extends Controller
{
    protected $view = 'review/myvotes.tpl';

    public function build()
    {
        $error = false;

        $info = $this->getParams();

        if(isset($info["url_arguments"])){
            if(!(sizeof($info["url_arguments"])==1 && $info["url_arguments"][0]==""))
                $error = true;
        }

        if($error)
            $this->setLayout('error/error404.tpl');
        else {

            $user = Session::getInstance()->get('user');

            if($user){

                $reviewModel = $this->getClass('ReviewReviewModel');
                $myVotes = $reviewModel->getMyPoints($user);

                if($myVotes) {
                    $this->delete($user);
                    $this->assign('myVotes',$myVotes);
                    $this->assign('votes',true);
                }

                $this->setLayout($this->view);

            }
            else {
                $this->setLayout('error/error403.tpl');
            }


        }

    }

    private function delete($user) {

        $delete = Filter::getBoolean('delete');
        $id = Filter::getInteger('IDdelete');

        if($delete) {
            $reviewModel = $this->getClass('ReviewReviewModel');
            $reviewModel->deleteVote($id,$user);
        }
    }

        /**
         * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
         * The sintax is the following:
         * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
         *
         * @return array
         */
        public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
    }
