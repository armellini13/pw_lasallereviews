<?php
/**
 * New Controller
 */
class ReviewNewController extends Controller
{
    protected $view = 'review/new.tpl';

    public function build()
    {
        $error = false;

        $info = $this->getParams();

        if(isset($info["url_arguments"])) {
            if(!(sizeof($info["url_arguments"])==1 && $info["url_arguments"][0]==""))
                $error = true;
        }

        if($error)
            $this->setLayout('error/error404.tpl');
        else {

            $user = Session::getInstance()->get('user');

            if(!$user) {
                $this->setLayout('error/error403.tpl');
            }
            else {

                // Agafo la informació
                $title = htmlspecialchars(Filter::getString('titleReview'));
                $description = Filter::getString('descriptionReview');
                $class = htmlspecialchars(Filter::getString('classReview'));
                $date = htmlspecialchars(Filter::getString('dateReview'));
                $score = htmlspecialchars(Filter::getString('scoreReview'));
                $photo = false;
                $savedInTemporal = Filter::getInteger('savedInTemporal');
                $tempPhotoPath = Filter:: getString('tempPhotoPath');
                $tempPhotoName = Filter::getString('tempPhotoName');

                if(isset($_FILES['photoReview']))
                    $photo = $_FILES['photoReview'];

                // Array d'informació en cas que haguem de tornar-la perquè hi han errors
                $inputInfo = array(
                    "title"   => $title,
                    "description"  => $description,
                    "class"  => $class,
                    "date" => $date,
                    "score" => $score,
                    "photo" => $photo,
                    "savedInTemporal" => $savedInTemporal,
                    "tempPhotoPath" => $tempPhotoPath,
                    "tempPhotoName" => $tempPhotoName,

                );

                // Array d'errors, en principi no hi ha errors
                $inputErrors = array(
                    "title"   => false,
                    "description"  => false,
                    "class"  => false,
                    "date" => false,
                    "titleEmpty" => false,
                    "photo" => false,
                    "photoEmpty" => false,
                    "photoSize" => false,
                    "titleUnique" => false,
                    "checked" => false,
                );

                // Filtro el formulari
                $uploaded = $this->filterForm($inputInfo, $inputErrors);

                if($uploaded)
                    $this->uploadReview($inputInfo);
                else {
                    if(!($inputErrors['photo'] || $inputErrors['photoSize'] || $inputErrors['photoEmpty']) && $inputErrors['checked'] && !$inputInfo['savedInTemporal']) {
                        $tempPhotoInfo = $this->saveInTemporal($inputInfo);
                        $inputInfo['tempPhotoName'] = $tempPhotoInfo['name'];
                        $inputInfo['tempPhotoPath'] = $tempPhotoInfo['path'];
                        $inputInfo['savedInTemporal'] = true;
                    }
                }

                $this->assign('inputError',$inputErrors);
                $this->assign('inputInfo',$inputInfo);
                $this->assign('uploaded',$uploaded);
                $this->setLayout($this->view);
            }
        }
    }

    // Penja la review
    private function uploadReview($inputInfo) {

        // Agafo el model
        $reviewModel = $this->getClass('ReviewReviewModel');
        $userModel = $this->getClass('UserUserModel');

        // Generem el codi únic
        $id = rand(0,999999);

        // Guardem la foto
        if($inputInfo['savedInTemporal']) {
            $photo = $id.'.'.pathinfo($inputInfo['tempPhotoName'], PATHINFO_EXTENSION);
            $path = PATH_HTDOCS.'imag/uploads/'.$photo;
            rename($inputInfo['tempPhotoPath'], $path);
            // Re-dimensionem la foto
            $this->resizePhoto($path,pathinfo($inputInfo['tempPhotoName'], PATHINFO_EXTENSION),$photo);
        }
        else {
            $photo = $id.'.'.pathinfo($inputInfo['photo']['name'], PATHINFO_EXTENSION);
            $path = PATH_HTDOCS.'imag/uploads/'.$photo;
            move_uploaded_file($inputInfo['photo']['tmp_name'], $path);
            // Re-dimensionem la foto
            $this->resizePhoto($path,pathinfo($inputInfo['photo']['name'], PATHINFO_EXTENSION),$photo);
        }

        // Agafem a l'usuari
        $author = Session::getInstance()->get('user');

        // Agafem el login de l'usuari
        $login = $userModel->getLogin($author);

        // Agafem la URL
        $url = strtolower(str_replace(" ","-",preg_replace("/[^ \w]+/", "",$inputInfo['title'])));

        // Afegeix l'usuari a la BBDD
        $reviewModel->uploadReview($id,$author,$login,$inputInfo['title'],$inputInfo['description'],
            $inputInfo['class'],$inputInfo['date'],$inputInfo['score'],$photo,$url);
    }

    // Resizes the photo
    private function resizePhoto($uploadedfile,$extension,$photo) {

        // Miro el format de la imatge
        if($extension=="jpg" || $extension=="jpeg" )
            $src = imagecreatefromjpeg($uploadedfile);

        else {
            if($extension=="png")
                $src = imagecreatefrompng($uploadedfile);
            else
                $src = imagecreatefromgif($uploadedfile);
        }

        // Agafem la mida de la foto
        list($width,$height) = getimagesize($uploadedfile);

        // Determinem les dues mides
        $newwidth1 = 704;
        $newheight1 = 528;

        $newwidth2 = 100;
        $newheight2 = 100;


        // Copiem la foto antiga a la nova amb nova dimensió
        $tmp1 =imagecreatetruecolor($newwidth1,$newheight1);
        $tmp2 =imagecreatetruecolor($newwidth2,$newheight2);

        imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,
            $width,$height);
        imagecopyresampled($tmp2,$src,0,0,0,0,$newwidth2,$newheight2,
            $width,$height);

        // Eliminem la foto antiga
        unlink($uploadedfile);

        // Camins a les carpetes
        $path1 = PATH_HTDOCS.'imag/uploads/large/'.$photo;
        $path2 = PATH_HTDOCS.'imag/uploads/small/'.$photo;

        // Creem la nova
        if($extension=="jpg" || $extension=="jpeg" ) {
            imagejpeg($tmp1,$path1,100);
            imagejpeg($tmp2,$path2,100);
        }
        else {
            if($extension=="png") {
                imagepng($tmp1,$path1,100);
                imagepng($tmp2,$path2,100);
            }
            else {
                imagegif($tmp1,$path1,100);
                imagegif($tmp2,$path2,100);
            }
        }

        imagedestroy($src);
        imagedestroy($tmp1);
        imagedestroy($tmp2);
    }

    // Guardo la foto en temporal
    private function saveInTemporal($inputInfo) {

        // Guardem la foto a la carpeta temporal
        $photo = $inputInfo['photo']['name'];
        $path = PATH_HTDOCS.'imag/tmp/'.$photo;
        move_uploaded_file($inputInfo['photo']['tmp_name'], $path);

        $photoInfo = array(
            "path" => $path,
            "name" => $photo,
        );

        return $photoInfo;
    }



    // Filtra el formulari
    private function filterForm(&$inputInfo,&$inputErrors) {

        if($inputInfo['title'] || $inputInfo['description'] || $inputInfo['class'] ||
            $inputInfo['date'] ||$inputInfo['score'] || !empty($inputInfo['photo'])) {

            $reviewModel = $this->getClass('ReviewReviewModel');

            // Miro si hi ha error al títol
            $inputInfo['title'] = preg_replace('!\s+!', ' ',ltrim(rtrim($inputInfo['title'])));

            if($inputInfo['title']) {
                if(strlen($inputInfo['title'])>100)
                    $inputErrors['title'] = true;
                else {
                    // Miro si el títol és únic
                    if(!$reviewModel->titleUnique($inputInfo['title']))
                        $inputErrors['titleUnique'] = true;
                }
            }
            else
                $inputErrors['titleEmpty'] = true;

            // Miro si hi ha error a la descripcio
            $inputInfo['description'] = ltrim(rtrim($inputInfo['description']));


            if(!strip_tags($inputInfo['description']))
                $inputErrors['description'] = true;

            // Miro si hi ha errors a la classe
            $inputInfo['class'] = preg_replace('!\s+!', ' ', ltrim(rtrim($inputInfo['class'])));

            if(!$inputInfo['class'])
                $inputErrors['class'] = true;

            // Miro si hi ha errors a la data
            if(!$this->validDateFormat($inputInfo['date']))
                $inputErrors['date'] = true;

            // Miro si hi ha errors a la foto
            // Si ja està guardada a temporal no miro errors
            if(!$inputInfo['savedInTemporal']) {
                // Miro si hi ha foto
                if(!(empty($inputInfo['photo']['name'])) || !$inputInfo['photo']['error']){
                    // Miro si la mida es correcta
                    if($inputInfo['photo']['size'] <= 2097152){
                        // Miro el format de la foto
                        $allowed =  array('gif','png' ,'jpg');
                        $photoName = $inputInfo['photo']['name'];
                        $ext = pathinfo($photoName, PATHINFO_EXTENSION);
                        if(!in_array($ext,$allowed) )
                            $inputErrors['photo'] = true;
                    }
                    else
                        $inputErrors['photoSize'] = true;
                }
                else
                    $inputErrors['photoEmpty'] = true;
            }

            // Flag d'error
            $errorInsideArray = false;
            // Miro si hi ha errors activats a l'array
            foreach ($inputErrors as $errorValue) {
                if($errorValue)
                    $errorInsideArray = true;
            }

            $inputErrors['checked'] = true;

            // Si hi ha errors es torna la informació
            if($errorInsideArray)
                return false;

            else
                return true;
        }

        return false;
    }

    private function validDateFormat($date) {
        return (DateTime::createFromFormat('d/m/Y', $date) !== FALSE);
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}