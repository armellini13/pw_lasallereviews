<?php
/**
 * New Controller
 */
class ReviewMyReviewsController extends Controller
{
    protected $view = 'review/myreviews.tpl';

    public function build()
    {
        $error = false;
        $page = 0;
        $totalPages = 0;

        $info = $this->getParams();

        $user = Session::getInstance()->get('user');

        if(isset($info["url_arguments"])) {

            if(sizeof($info["url_arguments"])==1) {

                $reviewModel = $this->getClass('ReviewReviewModel');

                if($info["url_arguments"][0]!="") {

                    if(is_numeric($info["url_arguments"][0])) {

                        if($user) {
                            $totalPages = ceil($reviewModel->getMyReviewSize($user) / 10);

                            if($info["url_arguments"][0] >= 0 && $info["url_arguments"][0] < $totalPages) {
                                $page = $info["url_arguments"][0];
                            }
                            else
                                $error = true;
                        }
                        else
                            $error = true;
                    }
                    else {
                        $error = true;
                    }
                }
                else {
                    if($user)
                        $totalPages = ceil($reviewModel->getMyReviewSize($user) / 10);
                }
            }
            else
                $error = true;
        }
        else {
            $reviewModel = $this->getClass('ReviewReviewModel');
            if($user)
                $totalPages = ceil($reviewModel->getMyReviewSize($user) / 10);
        }

        if($error)
            $this->setLayout('error/error404.tpl');
        else {

            if(!$user) {
                $this->setLayout('error/error403.tpl');
            }
            else {

                $this->delete();

                // Retorna la informació del review si s'ha clickat edit
                // Activa el flag de modificar a true
                $edit = $this->edit();
                $this->assign('edit',$edit);

                if($edit) {
                    if($edit['ID']!= "") {
                        $this->assign('edit',true);
                        $this->assign('inputInfo',$edit);
                    }
                    else {
                        // Array d'errors, en principi no hi ha errors
                        $inputErrors = array(
                            "title"   => false,
                            "description"  => false,
                            "class"  => false,
                            "date" => false,
                            "titleEmpty" => false,
                            "photo" => false,
                            "photoEmpty" => false,
                            "photoSize" => false,
                            "titleUnique" => false,
                        );
                        $uploaded = $this->modify($inputErrors);
                        if($uploaded) {
                            $this->assign('edit',true);
                            $this->assign('inputError',$inputErrors);
                            $this->assign('inputInfo',$uploaded);
                        }
                        else {

                            $this->assign('edit',false);
                            $reviewArray = $reviewModel->getMyReviews($user,$page);
                            $this->assign('totalPages',$totalPages);
                            $this->assign('page',$page+1);
                            $this->assign('previous',$page-1);
                            $this->assign('reviewArray',$reviewArray);
                        }
                    }
                }
                else {
                    $reviewArray = $reviewModel->getMyReviews($user,$page);
                    $this->assign('totalPages',$totalPages);
                    $this->assign('page',$page+1);
                    $this->assign('previous',$page-1);
                    $this->assign('reviewArray',$reviewArray);
                }

                $this->setLayout($this->view);

            }
        }
    }

    // Penja la review
    private function updateReview($inputInfo) {

        // Agafo el model
        $reviewModel = $this->getClass('ReviewReviewModel');
        $userModel = $this->getClass('UserUserModel');

        // Guardem la foto
        if(!empty($inputInfo['photo']['name'])){
            $photo = $inputInfo['ID'].'.'.pathinfo($inputInfo['photo']['name'], PATHINFO_EXTENSION);
            $path = PATH_HTDOCS.'imag/uploads/'.$photo;
            $pathDelete1 = PATH_HTDOCS.'imag/uploads/large/'.$photo;
            $pathDelete2 = PATH_HTDOCS.'imag/uploads/small/'.$photo;
            unlink($pathDelete1);
            unlink($pathDelete2);
            move_uploaded_file($inputInfo['photo']['tmp_name'], $path);
            // Re-dimensionem la foto
            $this->resizePhoto($path,pathinfo($inputInfo['photo']['name'], PATHINFO_EXTENSION),$photo);
        }
        else
            $photo = "";

        // Agafem la URL
        $url = strtolower(str_replace(" ","-",preg_replace("/[^ \w]+/", "",$inputInfo['title'])));

        // Afegeix l'usuari a la BBDD
        $reviewModel->updateReview($inputInfo['ID'],$inputInfo['title'],$inputInfo['description'],
            $inputInfo['class'],$inputInfo['score'],$photo,$url);
    }

    private function modify(&$inputErrors) {

        // Agafo la informació
        $title = htmlspecialchars(Filter::getString('titleReview'));
        $description = Filter::getString('descriptionReview');
        $class = htmlspecialchars(Filter::getString('classReview'));
        $date = htmlspecialchars(Filter::getString('dateReview'));
        $score = htmlspecialchars(Filter::getString('scoreReview'));
        $id = Filter::getInteger('id');
        $oldPhoto = Filter::getString('oldPhoto');
        $photo = "";
        $photoFormat = Filter::getBoolean('photoFormatReview');

        echo $oldPhoto;

        if(isset($_FILES['photoReview']))
            $photo = $_FILES['photoReview'];

        // Array d'informació en cas que haguem de tornar-la perquè hi han errors
        $inputInfo = array(
            "ID" => $id,
            "title"   => $title,
            "description"  => $description,
            "class"  => $class,
            "date" => $date,
            "score" => $score,
            "photo" => $photo,
            "photoFormat" => $photoFormat,
            "oldPhoto" => $oldPhoto,
        );


        // Filtro el formulari
        $uploaded = $this->filterForm($inputInfo,$inputErrors);

        if($uploaded) {
            $this->updateReview($inputInfo);
            $uploaded = false;
        }
        else {
            $uploaded = $inputInfo;
        }

        return $uploaded;
    }

    // Resizes the photo
    private function resizePhoto($uploadedfile,$extension,$photo) {

        // Miro el format de la imatge
        if($extension=="jpg" || $extension=="jpeg" )
            $src = imagecreatefromjpeg($uploadedfile);

        else {
            if($extension=="png")
                $src = imagecreatefrompng($uploadedfile);
            else
                $src = imagecreatefromgif($uploadedfile);
        }

        // Agafem la mida de la foto
        list($width,$height) = getimagesize($uploadedfile);

        // Determinem les dues mides
        $newwidth1 = 704;
        $newheight1 = 528;

        $newwidth2 = 100;
        $newheight2 = 100;


        // Copiem la foto antiga a la nova amb nova dimensió
        $tmp1 =imagecreatetruecolor($newwidth1,$newheight1);
        $tmp2 =imagecreatetruecolor($newwidth2,$newheight2);

        imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,
            $width,$height);
        imagecopyresampled($tmp2,$src,0,0,0,0,$newwidth2,$newheight2,
            $width,$height);

        // Eliminem la foto antiga
        unlink($uploadedfile);

        // Camins a les carpetes
        $path1 = PATH_HTDOCS.'imag/uploads/large/'.$photo;
        $path2 = PATH_HTDOCS.'imag/uploads/small/'.$photo;

        // Creem la nova
        if($extension=="jpg" || $extension=="jpeg" ) {
            imagejpeg($tmp1,$path1,100);
            imagejpeg($tmp2,$path2,100);
        }
        else {
            if($extension=="png") {
                imagepng($tmp1,$path1,100);
                imagepng($tmp2,$path2,100);
            }
            else {
                imagegif($tmp1,$path1,100);
                imagegif($tmp2,$path2,100);
            }
        }

        imagedestroy($src);
        imagedestroy($tmp1);
        imagedestroy($tmp2);
    }

    // Filtra el formulari
    private function filterForm(&$inputInfo,&$inputErrors) {


        if($inputInfo['title'] || $inputInfo['description'] || $inputInfo['class'] ||
            $inputInfo['date'] ||$inputInfo['score'] || !empty($inputInfo['photo'])) {

            $reviewModel = $this->getClass('ReviewReviewModel');

            // Miro si hi ha error al títol
            $inputInfo['title'] = preg_replace('!\s+!', ' ',ltrim(rtrim($inputInfo['title'])));

            if($inputInfo['title']) {
                if(strlen($inputInfo['title'])>100)
                    $inputErrors['title'] = true;
                else {
                    // Miro si el títol és únic
                    if(!$reviewModel->titleUniqueID($inputInfo['title'],$inputInfo['ID']))
                        $inputErrors['titleUnique'] = true;
                }
            }
            else
                $inputErrors['titleEmpty'] = true;

            // Miro si hi ha error a la descripcio
            $inputInfo['description'] = ltrim(rtrim($inputInfo['description']));


            if(!$inputInfo['description'])
                $inputErrors['description'] = true;

            // Miro si hi ha errors a la classe
            $inputInfo['class'] = preg_replace('!\s+!', ' ', ltrim(rtrim($inputInfo['class'])));

            if(!$inputInfo['class'])
                $inputErrors['class'] = true;

            // Miro si hi ha errors a la data
            if(!$this->validDateFormat($inputInfo['date']))
                $inputErrors['date'] = true;

            // Miro si hi ha errors a la foto
            // Miro si hi ha foto
            if(!(empty($inputInfo['photo']['name'])) || $inputInfo['photo']['error']==0) {
                // Miro si la mida es correcta
                if($inputInfo['photo']['size'] <= 2097152){
                    // Miro el format de la foto
                    $allowed =  array('gif','png' ,'jpg');
                    $photoName = $inputInfo['photo']['name'];
                    $ext = pathinfo($photoName, PATHINFO_EXTENSION);
                    if(!in_array($ext,$allowed) )
                        $inputErrors['photo'] = true;
                }
                else
                    $inputErrors['photoSize'] = true;
            }

            // Flag d'error
            $errorInsideArray = false;
            // Miro si hi ha errors activats a l'array
            foreach ($inputErrors as $errorValue) {
                if($errorValue)
                    $errorInsideArray = true;
            }
            // Si hi ha errors es torna la informació
            if($errorInsideArray)
                return false;

            else
                return true;
        }
    }

    private function validDateFormat($date) {
        return (DateTime::createFromFormat('d/m/Y', $date) !== FALSE);
    }


    private function edit() {
        $edit = Filter::getBoolean('edit');
        $id =  Filter::getInteger('IDedit');

        if($edit){
            if($id) {
                $reviewModel = $this->getClass('ReviewReviewModel');
                $inputInfo = $reviewModel->getReviewById($id);
                return $inputInfo;
            }
            else {

                $inputInfo = array("ID"=>"");

                return $inputInfo;
            }
        }
        else
            return false;
    }

    private function delete() {

        $delete = Filter::getBoolean('delete');
        $id = Filter::getInteger('IDdelete');

        if($delete) {
            $reviewModel = $this->getClass('ReviewReviewModel');
            $reviewModel->deleteReview($id);
        }
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}