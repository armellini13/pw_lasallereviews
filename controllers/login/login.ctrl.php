<?php
/**
 * Home Controller: Login de la pàgina.
 */
include('facebook/facebook.php');

class LoginLoginController extends Controller
{
    protected $view = 'login/login.tpl';

    public function build()
    {
        $error = false;

        $info = $this->getParams();

        if(isset($info["url_arguments"])) {
            if(!(sizeof($info["url_arguments"])==1 && $info["url_arguments"][0]=="")){
                    $error = true;
            }
        }

        if($error)
            $this->setLayout('error/error404.tpl');
        else {

            $logged = false;

            $user = Session::getInstance()->get('user');

            if($user)
                $logged = true;

            $this->logOut();
            $errorFacebook = $this->facebookLogin();
            $errorLog = $this->logIn();

            $this->assign('user',$user);
            $this->assign('logged',$logged);
            $this->assign('errorLog',$errorLog);
            $this->setLayout($this->view);
        }

    }

    private function logOut() {

        $logout2 = Filter::getString('logout2');
        $logout = Filter::getString('logout');

        if($logout || $logout2){
            if(Session::getInstance()->get('user'))
                Session::getInstance()->delete('user');
            if($logout)
                header('Location: http://gagus.local/home');
            else
                header('Location: http://gagus.local/login');
        }
    }

    //
    private function logIn() {

        $email = Filter::getEmail('email');
        $password = md5(Filter::getString('password'));

        if($email && $password) {

            $userModel = $this->getClass('UserUserModel');
            $user = $userModel->logUser($email,$password);

            if($user['name']){
                Session::getInstance()->set('user',$user['name']);
                header('Location: http://gagus.local/home');
            }
            else
                return true;
        }
        else
            if($password != md5('password'))
                return true;
            else
                return false;
        return false;
    }

    private function facebookLogin() {

        $facebook = Filter::getBoolean('facebook');

        if($facebook){

            // Info app
            $appid      = "854370327925065";
            $appsecret  = "78f0dfcf1be034072d82e5713c8ec70d";
            $facebook   = new Facebook(array(
                'appId' => $appid,
                'secret' => $appsecret,
                'cookie' => TRUE,
            ));

            // Cojo al usuario
            $fbuser = $facebook->getUser();

            // Si existe intento coger el perfil
            if ($fbuser) {
                try {
                    $user_profile = $facebook->api('/me');
                }
                catch (Exception $e) {
                    echo $e->getMessage();
                    exit();
                }

                $email = $user_profile["email"];

                $userModel = $this->getClass('UserUserModel');
                $user = $userModel->logUserFacebook($email);

                if($user['name']){
                    Session::getInstance()->set('user',$user['name']);
                    header('Location: http://gagus.local/home');
                }
                else {
                    return true;
                }

            }
        }

        return false;
    }



    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}