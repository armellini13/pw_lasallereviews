<?php
/**
 * Register Controller: Permet registrar a l'usuari.
 */
include_once('facebook/facebook.php');

class LoginRegisterController extends Controller
{
    protected $view = 'login/register.tpl';

    public function build()
    {
        $info = $this->getParams();
        $error = false;
        $confirmed = false;
        $registered = false;
        $facebook = false;

        // Compruevo URL
        if(isset($info["url_arguments"])) {
            if((sizeof($info["url_arguments"])==1)) {
                if($info["url_arguments"][0]!="") {

                    if($info["url_arguments"][0]!="fb") {
                        // Agafo el model
                        $userModel = $this->getClass('UserUserModel');

                        // Miro si l'usuari està confirmat o no
                        if($userModel->userConfirmed($info["url_arguments"][0]))
                            $error = true;
                        else
                            $confirmed = true;
                    }
                    else {
                        $facebook = true;
                    }

                }
            }
            else
                if(sizeof($info["url_arguments"])!=0)
                    $error = true;
        }

        if($error)
            $this->setLayout('error/error404.tpl');

        else {

            $inputErrors = null;

            if($confirmed) {

                // Confirmo usuari
                $name = $this->confirmUser($info["url_arguments"][0],$userModel);

                // Informació del welcome message
                $inputInfo = array(
                    "name"   => $name,
                );

                // Log in usuari
                Session::getInstance()->set('user',$name);
            }

            else {

                $inputErrors = array(
                    "name"   => false,
                    "login"  => false,
                    "email"  => false,
                    "password" => false,
                    "nameEmpty" => false,
                    "loginEmpty" => false,
                    "loginNoValid" => false,
                    "emailEmpty" => false,
                );

                if($facebook){
                    // Info app
                    $appid      = "854370327925065";
                    $appsecret  = "78f0dfcf1be034072d82e5713c8ec70d";
                    $facebook   = new Facebook(array(
                        'appId' => $appid,
                        'secret' => $appsecret,
                        'cookie' => TRUE,
                    ));

                    // Cojo al usuario
                    $fbuser = $facebook->getUser();

                    // Si existe intento coger el perfil
                    if ($fbuser) {
                        try {
                            $user_profile = $facebook->api('/me');
                        }
                        catch (Exception $e) {
                            echo $e->getMessage();
                            exit();
                        }
                        // Cojo
                        $user_email = $user_profile["email"];
                        $user_fnmae = $user_profile["first_name"];

                        // Array d'informació en cas que haguem de tornar-la perquè hi han errors
                        $inputInfo = array(
                            "name"   => $user_fnmae,
                            "login"  => "",
                            "email"  => $user_email,
                            "password" => "",
                        );

                    }
                }
                else {

                    // Agafo la informació
                    $name = htmlspecialchars(Filter::getString('nameRegister'));
                    $login = htmlspecialchars(Filter::getString('loginRegister'));
                    $email = Filter::getEmail('emailRegister');
                    $password = htmlspecialchars(Filter::getString('passwordRegister'));

                    // Array d'informació en cas que haguem de tornar-la perquè hi han errors
                    $inputInfo = array(
                        "name"   => $name,
                        "login"  => $login,
                        "email"  => $email,
                        "password" => $password,
                    );

                }

                // Filtro el formulari
                $registered = $this->filterForm($inputInfo,$inputErrors);

                if($registered)
                    $this->registerUser($inputInfo);
                else {
                    if(!$facebook)
                        $inputInfo['email'] = Filter::getString('emailRegister');

                }

            }
            $this->assign('facebook',$facebook);
            $this->assign('inputError',$inputErrors);
            $this->assign('confirmed',$confirmed);
            $this->assign('registered',$registered);
            $this->assign('inputInfo',$inputInfo);
            $this->setLayout($this->view);

        }

    }

    // Confirma l'usuari a la plataforma
    private function confirmUser($code,$userModel) {

        // Confirmo usuari i retorno la seva informació
        $nameUser= $userModel->confirmUser($code);

        return $nameUser;
    }

    // Registra a un usuari de forma temporal
    private function registerUser($inputInfo) {

        // Agafo el model
        $userModel = $this->getClass('UserUserModel');

        // Generem el codi únic
        $code = rand(1111,9999);

        // Afegeix l'usuari a la BBDD
        $userModel->registerTemporaryUser($inputInfo['name'],$inputInfo['login'],
                                            $inputInfo['email'],md5($inputInfo['password']),$code);

        // Envia l'email amb el link de confirmació
        $this->sendEmail($inputInfo,$code);

        $this->assign('code',$code);

    }

    // Envia l'email amb el link de confirmació
    private function sendEmail($inputInfo,$code) {

        $cuerpo  = '<h1>Mail de activacion</h1>';
        $cuerpo .= 'Enlace: <a href="gagus.local/signup/' . $code . '" >' . 'gagus.local/signup/' . $code . '</a>';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: registro@lasallereviews.com\r\n";

        mail($inputInfo['email'],"Registro LaSalle Reviews", $cuerpo, $headers);

    }

    // Filtra el formulari
    private function filterForm($inputInfo,&$inputErrors) {

        if($inputInfo['name'] || $inputInfo['login'] || $inputInfo['email'] || $inputInfo['password']) {

            // Agafo el model
            $userModel = $this->getClass('UserUserModel');

            // Miro si hi ha error al nom
            // Comprovo que hi hagi nom
            if($inputInfo['name']){
                // Miro si el nom està registrat
                if($userModel->nameRegistered($inputInfo['name']))
                 $inputErrors['name'] = true;
            }
            else
                $inputErrors['nameEmpty'] = true;

            // Miro si hi ha error al login
            // Miro si hi ha login
            if($inputInfo['login']) {
                // Miro si el login és vàlid
                if($this->validLogin($inputInfo['login'])){
                    // Miro si el login està registrat
                    if($userModel->loginRegistered($inputInfo['login']))
                       $inputErrors['login'] = true;
                }
                else
                    $inputErrors['loginNoValid'] = true;
            }
            else
                $inputErrors['loginEmpty'] = true;

            // Miro si hi ha error a l'email
            // Miro si hi ha email
            if($inputInfo['email']) {
                // Miro si l'email està registrat
                if($userModel->emailRegistered($inputInfo['email']))
                    $inputErrors['email'] = true;
            }

            else
                $inputErrors['emailEmpty'] = true;

            // Miro si hi ha error a la password
            // Miro si la password és vàlida
            if(strlen($inputInfo['password']) < 6 || strlen($inputInfo['password']) > 20)
                $inputErrors['password'] = true;

            // Flag d'error
            $errorInsideArray = false;
            // Miro si hi ha errors activats a l'array
            foreach ($inputErrors as $errorValue) {
                if($errorValue)
                    $errorInsideArray = true;
            }

            // Si hi ha errors es torna la informació
            if($errorInsideArray)
                return false;

            else
                return true;
        }

        else
            return false;
    }

    // Comprova que el login segueixi el format correcte
    private function validLogin($login){
        return (preg_match("/^[a-zA-Z]{2}+[0-9]{5}$/", $login));
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}