{$modules.head}
<div id="reviewBox">
<h1>Best Review</h1>
</div>
<div id="reviewBox" class="bestReview">
    <a id="nolink" href="{$url.global}/r/{$bestReview.URL}"> <h1 class="break" id="title">{$bestReview.title}</h1></a>
    <img src="{$url.global}/imag/uploads/small/{$bestReview.photo}">
    <p class="break"><label>Class: </label>{$bestReview.class}</p>
    <p class="break"><label>Date: </label>{$bestReview.date}</p>
    <p class="break"><label>Description: </label>{$bestReview.description}</p>
    <p class="break"><label>Author: </label>{$bestReview.author}</p>
    <p class="break"><label>Login: </label>{$bestReview.login}</p>
    <p><label>Score: </label>{$bestReview.score}</p>
    <p><label>Published: </label>{$bestReview.dateCreated}</p>
</div>
    <a class="twitter-timeline" href="https://twitter.com/search?q=%23projectesWeb14" data-widget-id="462055791529177089">Tweets sobre "#projectesWeb14"</a>

<div id="reviewBox">
    <h1>Last 10 Reviews</h1>
</div>
{   foreach from=$lastTen item=reviews}
<div id="reviewBox" class="bg">
    <a id="nolink" href="{$url.global}/r/{$reviews.URL}"><h1 class="break" id="title">{$reviews.title}</h1></a>
    <p class="break"><label>Date: </label>{$reviews.date}</p>
    <p class="break"><label>Description: </label>{$reviews.description|truncate:50}</p>
    <p><label>Score: </label>{$reviews.score}</p>
</div>
{   /foreach}

<div id="reviewBox">
    <h1>Last 10 Photos Uploaded</h1>
</div>
<div id="reviewBox">
{   foreach from=$lastPhotos item=photos}
    <img id="lastTen" src="{$url.global}/imag/uploads/small/{$photos.photo}">
{   /foreach}
</div>
{$modules.bestreviews}
{$modules.footer}