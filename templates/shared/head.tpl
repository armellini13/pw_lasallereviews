<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="La Salle Reviews" />
	<meta name="keywords" content="laSalle, reviews, university, teachers, students, opinion" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />		
	<title>LaSalle Reviews</title>
	<link rel="stylesheet" href="{$url.global}/css/style.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <link rel="shortcut icon" href="{$url.global}/imag/logo.ico">
    <script type="text/javascript" id="facebook-jssdk" src="http://connect.facebook.net/en_US/all.js"></script>
    <script src="{$url.global}/js/fb.js"></script>

</head>
<body>


    <header>
        <div class="logo">
            <a href="{$url.global}">
                <img src="{$url.global}/imag/logo.png">
                <h1>LaSalle Reviews</h1>
            </a>
        </div>
        <div class="searchBox">
            <form method="POST" action="/search">
                <input id="box" name="search" type="text" value="Comment, title.." onfocus="inputFocus(this)" onblur="inputBlur(this)">
                <input id="submit" type="submit" value="Search">
            </form>
        </div>
        {   if $logged}
        <div class="logged">
            <a href="{$url.global}/myreviews" id="user" id="login" class="options">{$user|truncate:20}</a>
            <form method="POST" action="/login">
                <input type="hidden" name="logout" value="true">
                <input id="logout" type="submit" value="Log out">
            </form>
        </div>
        {   else}
            <div class="login">
                <a href="#" id="login" class="options">Log in</a>
                <a href="{$url.global}/signup" id="signUp" class="options">Sign up</a>
                <div class="arrow"></div>
                <div class="boxLogin">
                    <form id="menuLogin" method="POST" action="/login">
                        <label for="box">User:</label> <input id="box" type="text" name="email" value="email" onfocus="inputFocus(this)" onblur="inputBlur(this)">
                        <label for="box">Password:</label>  <input id="box" type="password" name="password" value="password" onfocus="inputFocus(this)" onblur="inputBlur(this)">
                        <input id="submit" type="submit" value="Log In">
                    </form>
                    <form method="POST" action="/login">
                        <input type="hidden" name="facebook" value="true">
                        <input id="facebook" type="submit" value="Log in with Facebook">
                    </form>
                </div>
            </div>
        {   /if}
    </header>
    <header id="secondHeader">
        <div id="secondLogo">
            <img src="{$url.global}/imag/logo.png">
        </div>
        <ul id="secondMenu">
            {   if $logged}
                {   if $header[0] == 'n'}
                    <li class="active"><a href="{$url.global}/new">+New</a></li>
                {   else}
                    <li><a href="{$url.global}/new" >+New</a></li>
                {   /if}
            {   /if}
            {   if $header[0] == 'h' || $header[0] == 'd'}
                <li class="active"><a href="{$url.global}">Home</a></li>
            {   else}
                <li><a href="{$url.global}">Home</a></li>
            {   /if}
            {   if $header[0] == 'r'}
                <li class="active"><a href="{$url.global}/r"">Reviews</a></li>
            {   else}
                <li><a href="{$url.global}/r">Reviews</a></li>
            {   /if}
            {   if $logged}
                {   if $header[0] == 'm'}
                <li class="active"><a href="{$url.global}/myreviews">My Reviews</a></li>
                {   else}
                <li><a href="{$url.global}/myreviews">My Reviews</a></li>
                {   /if}
            {   /if}
            {   if $logged}
                {   if $header[0] == 'v'}
                <li class="active"><a href="{$url.global}/votes">My Votes</a></li>
                {   else}
                <li><a href="{$url.global}/votes">My Votes</a></li>
                {   /if}
            {   /if}
        </ul>
    </header>

	<div id="wrapper">