{   if $review}

    <div id="registerForm">
    {   if $registered}
         {  if $reviewInfo.npoints==0}
            <p>Nobody has voted this review yet.</p>
        {   else}
            <p>Points: {$reviewInfo.meanpoints}</p>
            <p>Users who voted: {$reviewInfo.npoints}</p>
        {   /if}
        {   if $myPoint}
            <p>My vote: {$myPoint}</p>
        {   else}
            <p>You still haven't voted this review.</p>

        {   /if}

        <br>
        <form class="newReview" method="POST" enctype="multipart/form-data" name="newReview">
                <select value="{$inputInfo.score}" class="inputRegister"  id="otherInput" name="pointReview">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <input class="inputRegister" type="submit" value="Submit" onfocus="inputFocus(this)" onblur="inputBlur(this)">
            </form>
    {   else}
        <p>You must be logged in to value reviews</p>
        <br>
        <a href="{$url.global}/login" id="facebook">Log In</a>
    {   /if}
    </div>
{   /if}

