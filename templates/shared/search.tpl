{$modules.head}
{   foreach from=$results item=reviews name=foo}
{   if ($smarty.foreach.foo.index >= $previous) && ($smarty.foreach.foo.index < $next)}
<div id="reviewBox">
    <h1 class="break" id="title">{$reviews.title}</h1>
    <img src="{$url.global}/imag/uploads/small/{$reviews.photo}">
    <p class="break"><label>Date: </label>{$reviews.date}</p>
    <p class="break"><label>Description: </label>{$reviews.description|truncate:50}</p>
    <p class="break"><label>Login: </label>{$reviews.login}</p>
    <p><label>Score: </label>{$reviews.score}</p>
</div>
{   /if}
{   /foreach}
{   if ($match!=0)&& $previous!=0 || $next!=0 }
<div id="nextPrev">
    {   if $previous!=0}
    <form method="POST">
        <input type="hidden" name="previous" value="{$previous}">
        <input type="hidden" name="search" value="{$search}">
        <input type="hidden" name="next" value="{$next}">
        <input type="hidden" name="side" value="previous">
        <input id="submit" type="submit" value="Previous">
    </form>
    {   /if}
    {   if $next!=0 && $next < $match}
    <form method="POST">
        <input type="hidden" name="previous" value="{$previous}">
        <input type="hidden" name="search" value="{$search}">
        <input type="hidden" name="next" value="{$next}">
        <input type="hidden" name="side" value="next">
        <input id="submit" type="submit" value="Next">
    </form>
    {   /if}
</div>
{   /if}
{   if $match==0 && !$no}
<div id="reviewBox">
    <h1>No results</h1>
</div>
{   /if}
{$modules.bestreviews}


{$modules.footer}