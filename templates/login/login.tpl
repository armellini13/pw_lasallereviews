{$modules.head}

<div id="loginForm">

    {   if $logged}
        <h1>Wait!</h1>
        <p>Seems to be {$user} is already logged in! Do you want to log out?</p>
        <form class="login" method="POST" id="logout">
            <input type="hidden" value="true" name="logout2">
            <input type="submit" value="Log Out">
        </form>
        <img src="{$url.global}/imag/user.png">
    {   else}
        <h1>Log In</h1>
        {   if $errorLog}
            <p id="errorMessage">Woops! Can't log in. Check your email or your password..</p>
        {   /if}
        <form class="login" method="POST">
            <label for="email">E-mail:</label>
            <input value="{$inputInfo.email}" name="email" type="text"">
            <label for="password">Password:</label>
            <input value="{$inputInfo.password}" name="password" type="password"">
            <input type="submit" name="submitLog" value="Log In">
        </form>
    {   /if}
</div>

{$modules.footer}