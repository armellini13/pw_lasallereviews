{$modules.head}

<div id="registerForm">
    {   if $confirmed}
    <div id="confirmation">
        <h1>Welcome!</h1>
        <p>Welcome <span>{$inputInfo.name}</span>! LaSalle Reviews is more than happy to see you have joined
            our community. Start posting and rating reviews <span>now</span>!
        </p>
        <br><br>
        <a href="{$url.global}/home">I'm Ready!</a>
        <img src="{$url.global}/imag/wine.png">
    </div>
    {   else}
        {   if !$registered}
        <h1>Join LaSalle Reviews today.</h1>
        <form class="register" method="POST" action="/signup">
            <label for="name">Name:</label>

            { if ($inputError.nameEmpty || $inputError.name) && !$facebook}
                <input class="inputRegister errorCaja" id="nameRegister" value="{$inputInfo.name}" name="nameRegister" type="text">
            {   else}
                <input class="inputRegister" id="nameRegister" value="{$inputInfo.name}" name="nameRegister" type="text">
            {   /if}
            {   if $inputError.nameEmpty}
                <p class="error">You have to introduce a valid name.</p>
            {   /if}
            {   if $inputError.name}
                <p class="error">This name is already chosen.</p>
            {   /if}

            <label for="login">Login:</label>
            { if ($inputError.loginEmpty || $inputError.login || $inputError.loginNoValid)&& !$facebook}
                <input class="inputRegister errorCaja"  id="loginRegister" value="{$inputInfo.login}" name="loginRegister" type="text"">
            {   else}
                <input class="inputRegister"  id="loginRegister" value="{$inputInfo.login}" name="loginRegister" type="text"">
            { /if}
            {   if !$facebook && ($inputError.loginEmpty || $inputError.loginNoValid)}
                <p class="error">You have to introduce a valid login.</p>
            {   /if}
            {   if $inputError.login}
            <p class="error">This login is already chosen.</p>
            {   /if}
            <label for="email">E-mail:</label>

            { if !$facebook &&($inputError.emailEmpty || $inputError.email)}
                <input class="inputRegister errorCaja" id="emailRegister" value="{$inputInfo.email}" name="emailRegister" type="text"">
            {   else}
            <input class="inputRegister" id="emailRegister" value="{$inputInfo.email}" name="emailRegister" type="text"">
            {   /if}
            {   if $inputError.emailEmpty}
                <p class="error">You have to introduce a valid e-mail.</p>
            {   /if}
            {   if $inputError.email}
            <p class="error">This e-mail is already chosen.</p>
            {   /if}


            <label for="password">Password:</label>
            { if !$facebook && $inputError.password}
            <input class="inputRegister errorCaja"  id="passwordRegister" value="{$inputInfo.password}" name="passwordRegister" type="password"">

            {   else}
                <input class="inputRegister" id="passwordRegister" value="{$inputInfo.password}" name="passwordRegister" type="password"">
            {   /if}
            {   if !$facebook && $inputError.password}
            <p class="error">The password needs to have between 6 and 20 characters.</p>
            {   /if}
            <input class="inputRegister" type="submit" value="Register" onfocus="inputFocus(this)" onblur="inputBlur(this)">
        </form>
        {   if !$facebook}
            <a href="javascript:FBRegister();" id="registerFacebook">Register with Facebook</a>
        {   /if}
        <a href="javascript:FBLogout();" >FB Log Out</a>
        {   else}
            <div id="confirmation">
                <h1>Awesome!</h1>
                <p>There is only one step more to follow if you want to be part of the LaSalle Reviews community. An
                   activation link has been sent to <span>{$inputInfo.email}</span>. Use that link to activate your account.
                </p>
                <a href="{$url.global}/signup/{$code}">Confirmation</a>
                <p>We will be here waiting for you!</p>
                <img src="{$url.global}/imag/target.png">
            </div>
        {   /if}
    {   /if}
</div>



{$modules.footer}