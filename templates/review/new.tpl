{$modules.head}
<script src="{$url.global}/js/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

<div id="newForm">
    {   if $uploaded}
        <h1>Review succesfully uploaded!</h1>
        <p>Follow this link in order to see your review!</p>
        <img src="{$url.global}/imag/box.png">
    {   else}
        <h1>New review</h1>
        <form class="newReview" method="POST" enctype="multipart/form-data" name="newReview">
            <label for="titleReview">Title:</label>
            {   if ($inputError.title || $inputError.titleUnique || $inputError.titleEmpty)}
                <input class="inputRegister errorCaja" value="{$inputInfo.title}" name="titleReview" type="text">
            {   else}
                <input class="inputRegister" value="{$inputInfo.title}" name="titleReview" type="text">
            {   /if}
            {   if $inputError.title}
                <p class="error">You have to introduce a valid title.</p>
            {   /if}
            {   if $inputError.titleEmpty}
                <p class="error">You have to introduce a title.</p>
            {   /if}
            {   if $inputError.titleUnique}
                <p class="error">This title already exists.</p>
            {   /if}

            <label for="descriptionReview">Description:</label>
            <textarea class="inputRegister" name="descriptionReview" id="descriptionReview" type="text"">
            {$inputInfo.description}
            </textarea>
            <br>
            {   if $inputError.description}
                <p class="error">You have to introduce a description.</p>
            {   /if}
            <label for="classReview">Class:</label>
            {   if $inputError.class}
            <input class="inputRegister errorCaja" value="{$inputInfo.class}" name="classReview" type="text"">
            {   else}
            <input class="inputRegister" value="{$inputInfo.class}" name="classReview" type="text"">
            {   /if}
            {   if $inputError.class}
                <p class="error">You have to introduce a class.</p>
            {   /if}
            <label for="dateReview">Date:</label>
            {   if $inputError.date}
                <input class="inputRegister errorCaja" value="{$inputInfo.date}" type="text" id="datepicker" name="dateReview"/>
            {   else}
                <input class="inputRegister" value="{$inputInfo.date}" type="text" id="datepicker" name="dateReview"/>
            {   /if}
            {   if $inputError.date}
                <p class="error">You have to introduce a valid date.</p>
            {   /if}
            <label for="scoreReview">Score:</label>
            <select value="{$inputInfo.score}" class="inputRegister"  id="otherInput" name="scoreReview">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
            <input class="inputRegister" id="photoInput" type="hidden" name="savedInTemporal" value="{$inputInfo.savedInTemporal}"/>
            <input class="inputRegister" id="photoInput" type="hidden" name="tempPhotoName" value="{$inputInfo.tempPhotoName}"/>
            <input class="inputRegister" id="photoInput" type="hidden" name="tempPhotoPath" value="{$inputInfo.tempPhotoPath}"/>
            {   if $inputInfo.savedInTemporal}
            <img src="{$url.global}/imag/tmp/{$inputInfo.tempPhotoName}" id="tempPhoto">
            {   else}
                <label for="photoReview">Photo:</label>
                <input class="inputRegister" id="photoInput" type="file" name="photoReview" value="{$inputInfo.photo.tmp_name}"/>
            {   if $inputError.photoEmpty}
                    <p class="error">You have to introduce a photo.</p>
                {   else}
                    {   if $inputError.photo}
                        <p class="error">Your photo has to be .gif, .jpeg or .png to be uploaded.</p>
                    {   /if}
                    {   if $inputError.photoSize}
                        <p class="error">Your photo is too big to be uploaded.</p>
                    {   /if}
                {   /if}
            {   /if}
            <input class="inputRegister" type="submit" value="Upload Review" onfocus="inputFocus(this)" onblur="inputBlur(this)">
        </form>
    {   /if}
</div>



{$modules.footer}