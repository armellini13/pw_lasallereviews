{$modules.head}

{   if $review}
        <div id="reviewBox" class="reviewBig">
        <h1 class="break" id="title">{$review.title}</h1>
        <img src="{$url.global}/imag/uploads/large/{$review.photo}">
        <p class="break"><label>Class: </label>{$review.class}</p>
        <p class="break"><label>Date: </label>{$review.date}</p>
        <p class="break"><label>Description: </label>{$review.description}</p>
        <p class="break"><label>Author: </label>{$review.author}</p>
        <p class="break"><label>Login: </label>{$review.login}</p>
        <p><label>Score: </label>{$review.score}</p>
        <p><label>Published: </label>{$review.dateCreated}</p>
        </div>
        {$modules.point}
{$modules.bestreviews}


{   else}
    {   foreach from=$reviewArray item=reviews}
    <div id="reviewBox">
        <h1 class="break" id="title">{$reviews.title}</h1>
        <img src="{$url.global}/imag/uploads/small/{$reviews.photo}">
        <p class="break"><label>Class: </label>{$reviews.class}</p>
        <p class="break"><label>Date: </label>{$reviews.date}</p>
        <p class="break"><label>Description: </label>{$reviews.description}</p>
        <p class="break"><label>Author: </label>{$reviews.author}</p>
        <p class="break"><label>Login: </label>{$reviews.login}</p>
        <p><label>Score: </label>{$reviews.score}</p>
        <p><label>Published: </label>{$reviews.dateCreated}</p>
        <a href="{$url.global}/r/{$reviews.URL}">Go to review!</a>
    </div>
    {   /foreach}
    <div id="nextPrev">
    {   if $previous >= 0}
        <a id="nextPrev" href="{$url.global}/r/{$previous}">Previous</a>
    {   /if}
    {   if $page < $totalPages}
        <a id="nextPrev" href="{$url.global}/r/{$page}">Next</a>
    {   /if}
    </div>
{   /if}
{$modules.footer}