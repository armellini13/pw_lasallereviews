{$modules.head}

{   if $edit}
<div id="newForm">
    <h1>Edit Review</h1>
    <form class="newReview" method="POST" enctype="multipart/form-data" name="newReview">
        <input type="hidden" name="id" value="{$inputInfo.ID}">
        <input type="hidden" name="edit" value="{$edit}">
        <label for="titleReview">Title:</label>
        {   if ($inputError.title || $inputError.titleUnique || $inputError.titleEmpty)}
        <input class="inputRegister errorCaja" value="{$inputInfo.title}" name="titleReview" type="text">
        {   else}
        <input class="inputRegister" value="{$inputInfo.title}" name="titleReview" type="text">
        {   /if}
        {   if $inputError.title}
        <p class="error">You have to introduce a valid title.</p>
        {   /if}
        {   if $inputError.titleEmpty}
        <p class="error">You have to introduce a title.</p>
        {   /if}
        {   if $inputError.titleUnique}
        <p class="error">This title already exists.</p>
        {   /if}

        <label for="descriptionReview">Description:</label>
        <textarea class="inputRegister" name="descriptionReview" id="descriptionReview" type="text"">
        {$inputInfo.description}
        </textarea>
        <br>
        {   if $inputError.description}
        <p class="error">You have to introduce a description.</p>
        {   /if}
        <label for="classReview">Class:</label>
        {   if $inputError.class}
        <input class="inputRegister errorCaja" value="{$inputInfo.class}" name="classReview" type="text"">
        {   else}
        <input class="inputRegister" value="{$inputInfo.class}" name="classReview" type="text"">
        {   /if}
        {   if $inputError.class}
        <p class="error">You have to introduce a class.</p>
        {   /if}
        <label for="dateReview">Date:</label>
        {   if $inputError.date}
        <input class="inputRegister errorCaja" value="{$inputInfo.date}" type="text" id="datepicker" name="dateReview"/>
        {   else}
        <input class="inputRegister" value="{$inputInfo.date}" type="text" id="datepicker" name="dateReview"/>
        {   /if}
        {   if $inputError.date}
        <p class="error">You have to introduce a valid date.</p>
        {   /if}
        <label for="scoreReview">Score:</label>
        <select value="{$inputInfo.score}" class="inputRegister"  id="otherInput" name="scoreReview">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
        </select>
        <input class="inputRegister" id="photoInput" type="hidden" name="savedInTemporal" value="{$inputInfo.savedInTemporal}"/>

        {   if !$inputInfo.photo|is_array}
            <input class="inputRegister" id="PUTA" type="hidden" name="oldPhoto" value="{$inputInfo.photo}"/>
            <img src="{$url.global}/imag/uploads/small/{$inputInfo.photo}" id="tempPhoto">
        {   else}
            <input class="inputRegister" id="photoInput" type="hidden" name="oldPhoto" value="{$inputInfo.oldPhoto}"/>
            <img src="{$url.global}/imag/uploads/small/{$inputInfo.oldPhoto}" id="tempPhoto">
        {   /if}
        <br>
        <label for="photoReview">Photo:</label>
        <input class="inputRegister" id="photoInput" type="file" name="photoReview" value="{$inputInfo.photo.tmp_name}"/>
        {   if $inputError.photoEmpty}
        <p class="error">You have to introduce a photo.</p>
        {   else}
        {   if $inputError.photo}
        <p class="error">Your photo has to be .gif, .jpeg or .png to be uploaded.</p>
        {   /if}
        {   if $inputError.photoSize}
        <p class="error">Your photo is too big to be uploaded.</p>
        {   /if}
        {   /if}
        <input class="inputRegister" type="submit" value="Upload Review" onfocus="inputFocus(this)" onblur="inputBlur(this)">
    </form>
</div>
{   else}
    {   foreach from=$reviewArray item=reviews}
    <div id="reviewBox">
        <div id="delete">
            <h1 class="break" id="title">{$reviews.title}</h1>
            <form method="POST" onsubmit="return confirm('Do you really want to delete the review?');">
                <input type="hidden" name="IDdelete" value="{$reviews.ID}">
                <input type="hidden" name="delete" value="true">
                <input type="submit" value="Delete">
            </form>
            <form method="POST" id="edit">
                <input type="hidden" name="IDedit" value="{$reviews.ID}">
                <input type="hidden" name="edit" value="true">
                <input type="submit" value="Edit">
            </form>

        </div>
        <img src="{$url.global}/imag/uploads/small/{$reviews.photo}">
        <p class="break"><label>Class: </label>{$reviews.class}</p>
        <p class="break"><label>Date: </label>{$reviews.date}</p>
        <p class="break"><label>Description: </label>{$reviews.description}</p>
        <p class="break"><label>Author: </label>{$reviews.author}</p>
        <p class="break"><label>Login: </label>{$reviews.login}</p>
        <p><label>Score: </label>{$reviews.score}</p>
        <p><label>Published: </label>{$reviews.dateCreated}</p>
        <a href="{$url.global}/r/{$reviews.URL}">Go to review!</a>
    </div>
    {   /foreach}
    <div id="nextPrev">
        {   if $previous >= 0}
        <a id="nextPrev" href="{$url.global}/myreviews/{$previous}">Previous</a>
        {   /if}
        {   if $page < $totalPages}
        <a id="nextPrev" href="{$url.global}/myreviews/{$page}">Next</a>
        {   /if}
    </div>
{   /if}
{$modules.footer}