{$modules.head}

{   if $votes}
    {   foreach from=$myVotes item=reviews}
    <div id="reviewBox" class="bg">
        <a id="nolink" href="{$url.global}/r/{$reviews.URL}"><h1 class="break" id="title">{$reviews.title}</h1></a>
        <form method="POST" onsubmit="return confirm('Do you really want to delete the review?');">
            <input type="hidden" name="IDdelete" value="{$reviews.ID}">
            <input type="hidden" name="delete" value="true">
            <input type="submit" value="Delete">
        </form>
        <p class="break"><label>Date: </label>{$reviews.date}</p>
        <p class="break"><label>Vote: </label>{$reviews.npoints}</p>
    </div>
    {   /foreach}
{   else}
<div id="reviewBox" class="bg">
    <h1 class="break" id="title">You still haven't voted</h1>
</div>
{   /if}

{$modules.footer}