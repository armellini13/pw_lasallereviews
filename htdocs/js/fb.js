/* FACEBOOK */

window.fbAsyncInit = function() {
    FB.init({
        appId      : '854370327925065', // replace your app id here
        cookie     : true,
        xfbml      : true,
        version    : 'v2.0'
    });
};

function FBRegister(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = "/signup/fb";
        }
    }, {scope: 'email'});
}


