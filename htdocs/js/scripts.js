/* Input functions */
/* Takes care there's of the values inside the inputs when there is not text */
function inputFocus(i){
    if(i.value==i.defaultValue){ i.value=""; i.style.color="#000"; }
}
function inputBlur(i){
    if(i.value==""){ i.value=i.defaultValue; i.style.color="#696969"; }
}

/* Displays the login box */
$( document ).ready(function() {

    $('#login').click(function(){
        if($(this).siblings('.arrow').is(':visible')){
            $(this).siblings('.arrow').fadeOut('fast');
            $(this).siblings('.boxLogin').fadeOut('fast');
        }
        else{
            $(this).siblings('.arrow').fadeIn();
            $(this).siblings('.boxLogin').fadeIn();
        }
    });


    $(window).bind('scroll', function(event) {

        event.preventDefault();

        if ($(window).scrollTop() >= 75) {

            $('#secondHeader').addClass('fixed');
            $('#secondLogo').addClass('fixedLogo');
            if (!$('#secondLogo').is(':visible'))
                $('#secondLogo').toggle('slide');
        }
        else {
            $('#secondHeader').removeClass('fixed');
            $('#secondLogo').removeClass('fixedLogo');
            $('#secondLogo').hide();
        }


    });

    $(document).mouseup(function (e) {
        var container = $('header .login .boxLogin');
        var container2 = $('header .arrow')
        if (!container.is(e.target) && !container2.is(e.target)// if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.fadeOut('fast');
            container2.fadeOut('fast');
        }
    });

});

/*  jQuery ready function. Specify a function to execute when the DOM is fully loaded.  */
$(document).ready(

    /* This is the function that will get executed after the DOM is fully loaded */
    function () {
        $( "#datepicker" ).datepicker({
            changeMonth: true,//this option for allowing user to select month
            changeYear: true, //this option for allowing user to select from year range
            dateFormat: 'dd/mm/yy'
        });
    }

);

/* Displays the login box */

$( document ).ready(function() {

    $('.register').submit(function(event){
        var error = false;
        var pattern = /^[A-Z0-9._%\-+]+@(?:[A-Z0-9\-]+\.)+(?:[A-Z]{2}|com|edu|org|net|biz|info|name|aero|biz|info|jobs|travel|museum|name|cat|asia|coop|jobs|mobi|tel|pro|arpa|gov|mil)$/i;

        // Comprovo el nom
        if($("input[name='nameRegister']").val().length == 0){
            $("input[name='nameRegister']").css('border-color','red');
            error = true;
        }
        else {
            $("input[name='nameRegister']").css('border-color','#BDBDBD');
        }

        // Comprovo login
        if($("input[name='loginRegister']").val().length == 0){
            $("input[name='loginRegister']").css('border-color','red');
            error = true;
        }
        else {
            $("input[name='loginRegister']").css('border-color','#BDBDBD');
        }

        // Comprovo email
        if(!pattern.test($("input[name='emailRegister']").val())){
            $("input[name='emailRegister']").css('border-color','red');
            error = true;
        }
        else {
            $("input[name='emailRegister']").css('border-color','#BDBDBD');
        }

        // Comprovo password
        if($("input[name='passwordRegister']").val().length == 0){
            $("input[name='passwordRegister']").css('border-color','red');
            error = true;
        }
        else {
            $("input[name='passwordRegister']").css('border-color','#BDBDBD');
        }

        if(error)
            return false;
        else
            return true;
    });

    $('.newReview').submit(function(event){

        var error = false;

        // Comprovo el nom
        if($("input[name='titleReview']").val().length == 0){
            $("input[name='titleReview']").css('border-color','red');
            error = true;
        }
        else {
            $("input[name='titleReview']").css('border-color','#BDBDBD');
        }

        // Comprovo el nom
        if($("input[name='classReview']").val().length == 0){
            $("input[name='classReview']").css('border-color','red');
            error = true;
        }
        else {
            $("input[name='classReview']").css('border-color','#BDBDBD');
        }

        if(error)
            return false;
        else
            return true;
    });
});




/* Twitter javascript */
!function(d,s,id){
    var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
    if(!d.getElementById(id)){
        js=d.createElement(s);
        js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);
    }
}(document,"script","twitter-wjs");

/* NicEdit */
nicEditors.findEditor( "#descriptionReview" ).setContent( 'some value' );


