<?php

/*
 * Model que s'encarrega de guardar i comprovar la info dels usuaris
 * que es registren a la pàgina web.
 */
class UserUserModel extends Model
{

/*****************************************************************************
/* REGISTER FUNCTIONS                                                        *
 *****************************************************************************
 */
    // Comprova si el nom està registrat
    // Retorna true si el nom existeix
    public function nameRegistered($name) {

        $sqlUser = <<<QUERY
SELECT
    name
FROM
    user
WHERE
    name = "$name";
QUERY;
        $sqlNewUser = <<<QUERY
SELECT
    name
FROM
    newuser
WHERE
    name = "$name";
QUERY;


        $registeredNamesUser = $this->getAll($sqlUser);
        $registeredNamesNewUser = $this->getAll($sqlNewUser);

        return !(empty($registeredNamesUser)&&empty($registeredNamesNewUser));
    }

    // Comprova si el login està registrat
    // Retorna true si el login existeix
    public function loginRegistered($login) {

        $sqlUser = <<<QUERY
SELECT
    login
FROM
    user
WHERE
    login = "$login";
QUERY;
        $sqlNewUser = <<<QUERY
SELECT
    login
FROM
    newuser
WHERE
    login = "$login";
QUERY;

        $registeredLoginsUser = $this->getAll($sqlUser);
        $registeredLoginsNewUser = $this->getAll($sqlNewUser);

        return !(empty($registeredLoginsUser)&&empty($registeredLoginsNewUser));
    }

    // Comprova si el nom està registrat
    // Retorna true si el nom existeix
    public function emailRegistered($email) {

        $sqlUser = <<<QUERY
SELECT
    email
FROM
    user
WHERE
    email = "$email";
QUERY;
        $sqlNewUser = <<<QUERY
SELECT
    email
FROM
    newuser
WHERE
    email = "$email";
QUERY;

        $registeredEmailsUser = $this->getAll($sqlUser);
        $registeredEmailsNewUser = $this->getAll($sqlNewUser);

        return !(empty($registeredEmailsUser)&&empty($registeredEmailsNewUser));
    }

    // Comprova si l'usuari està registrat
    // Retorna true si hi ha usuari
    public function confirmUser($code) {

        // Agafem la informació de newuser
        $sqlUser = <<<QUERY
SELECT
    *
FROM
    newuser
WHERE
    code = "$code";
QUERY;

        $userConfirmed = $this->getAll($sqlUser);

        $name = $userConfirmed[0]['name'];
        $login = $userConfirmed[0]['login'];
        $email = $userConfirmed[0]['email'];
        $password = $userConfirmed[0]['password'];

        // La posem a user
        $sql = <<<QUERY
INSERT INTO
    user
VALUES (?,?,?,?);
QUERY;

        $this->execute($sql,array($name,$login,$email,$password));

        // Eliminem la informació de newuser
        $sql = <<<QUERY
DELETE FROM
    newuser
WHERE
    code = "$code";
QUERY;

        $this->execute($sql);

        return $name;
    }

    // Afageix l'usuari a la taula newUser
    // Genera un codi únic per confimar el registre del usuari més endavant
    public function registerTemporaryUser($name,$login,$email,$password,$code) {

        $sql = <<<QUERY
INSERT INTO
    newuser
VALUES ('$name','$login','$email','$password','$code');
QUERY;

        $this->execute($sql);
    }

    // Comprova si l'usuari està registrat
    // Retorna true si hi ha usuari
    public function userConfirmed($code) {

        $sqlUser = <<<QUERY
SELECT
    name
FROM
    newuser
WHERE
    code = "$code";
QUERY;

        $userConfirmed = $this->getAll($sqlUser);

        return (empty($userConfirmed));
    }

/*****************************************************************************
/* LOGIN FUNCTIONS                                                           *
 *****************************************************************************
 */

    // Comprova si l'usuari està registrat
    // Retorna el nom si hi ha usuari
    public function logUser($email,$password) {

        $sqlUser = <<<QUERY
SELECT
    name
FROM
    user
WHERE
    email = "$email" AND password = '$password';
QUERY;

        $user = $this->getAll($sqlUser);

        if(empty($user))
            return false;
        else
            return $user[0];
    }

    // Comprova si l'usuari està registrat
    // Retorna el nom si hi ha usuari
    public function logUserFacebook($email) {

        $sqlUser = <<<QUERY
SELECT
    name
FROM
    user
WHERE
    email = "$email";
QUERY;

        $user = $this->getAll($sqlUser);

        if(empty($user))
            return false;
        else
            return $user[0];
    }

/*****************************************************************************
/* LOGIN FUNCTIONS                                                           *
 *****************************************************************************
 */

        // Retorna el login
        public function getLogin($name) {

            $sqlUser = <<<QUERY
SELECT
    login
FROM
    user
WHERE
    name = "$name";
QUERY;

            $user = $this->getAll($sqlUser);

            return $user[0]['login'];
        }
}