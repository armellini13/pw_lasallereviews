<?php

/*
 * Model que s'encarrega de guardar les reviews dels usuaris
 * de la pàgina web.
 */
class ReviewReviewModel extends Model
{
    /*****************************************************************************
    /* UPLOAD FUNCTIONS                                                          *
     *****************************************************************************
     */
    // Comprova si el nom està registrat
    // Retorna true si el nom existeix
    public function uploadReview($id,$author,$login,$title,$description,$class,$date,$score,$photo,$url) {

        $sql = <<<QUERY
INSERT INTO
    review
VALUES (?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,0,0);
QUERY;

        $this->execute($sql,array($id,$author,$login,$title,$description,$class,$date,$score,$photo,$url));

        $id = rand(0,9999);

        $sql = <<<QUERY
INSERT INTO
    lastphotos
VALUES ('$id','$photo',CURRENT_TIMESTAMP);
QUERY;

        $this->execute($sql);

    }

    // Retorna el número total de reviews
    public function getReviewSize() {

        $sql = <<<QUERY
SELECT
    count(*)
FROM
    review;
QUERY;

        return $this->getAll($sql)[0]['count(*)'];
    }

    // Retorna si el títol és únic
    public function titleUnique($title) {

        $sql = <<<QUERY
SELECT
    title
FROM
    review
WHERE
    title = '$title';
QUERY;
        $unique = $this->getAll($sql);

        return empty($unique);
    }

    // Retorna si el títol és únic
    public function titleUniqueID($title,$id) {

        $sql = <<<QUERY
SELECT
    title
FROM
    review
WHERE
    title = '$title' AND ID != '$id';
QUERY;
        $unique = $this->getAll($sql);

        return empty($unique);
    }

    // Retorna el número total de reviews d'un usuari
    public function getMyReviewSize($user) {

        $sql = <<<QUERY
SELECT
    count(*)
FROM
    review
WHERE
    author = '$user';
QUERY;

        return $this->getAll($sql)[0]['count(*)'];
    }

    // Retorna la review demanada
    public function getReview($url) {

        $sqlUser = <<<QUERY
SELECT
    *
FROM
    review
WHERE
    URL = '$url';
QUERY;

        $result = $this->getAll($sqlUser);
        if(isset($result[0]))
            return $result[0];
        else
            return false;
    }

    // Retorna review
    public function getReviewById($id) {

        $sqlUser = <<<QUERY
SELECT
    *
FROM
    review
WHERE
    ID = '$id';
QUERY;

        $result = $this->getAll($sqlUser);
        if(isset($result[0]))
            return $result[0];
        else
            return false;

    }

    // Retorna deu reviews
    public function getReviewsFrom($offset) {

        $offset *= 10;

        $sqlUser = <<<QUERY
SELECT
    *
FROM
    review
LIMIT
    10
OFFSET
    $offset;
QUERY;

        $result = $this->getAll($sqlUser);

        if(isset($result))
            return $result;
        else
            return false;
    }

    // Retorna deu reviews
    public function getMyReviews($user,$offset) {

        $offset *= 10;

        $sqlUser = <<<QUERY
SELECT
    *
FROM
    review
WHERE
    author = '$user'
LIMIT
    10
OFFSET
    $offset;
QUERY;

        $result = $this->getAll($sqlUser);

        if(isset($result))
            return $result;
        else
            return false;
    }


    // Elimina una review
    public function deleteReview($id) {

        $sql = <<<QUERY
DELETE FROM
    review
WHERE
    id = '$id';
QUERY;

        $this->execute($sql);

        $sql = <<<QUERY
DELETE FROM
    lastphotos
WHERE
    photo like '$id.%';
QUERY;

        $this->execute($sql);

        $sql = <<<QUERY
DELETE FROM
    vote
WHERE
    review ='$id';
QUERY;

        $this->execute($sql);


    }


    // Actualiza una review
    public function updateReview($id,$title,$description,$class,$score,$photo,$newUrl) {

        // Agafo la antiga url
        $sql = <<<QUERY
SELECT
    URL
FROM
    review
WHERE
    ID = '$id';
QUERY;


        $oldUrl = $this->getAll($sql)[0]['URL'];

        if($oldUrl != $newUrl){

            $urlID = rand(0,999999);

        // Introduim a la taula d'antigues URL
        $sql = <<<QUERY
INSERT INTO
    oldurl
VALUES (?,?,?);
QUERY;

        $this->execute($sql,array($urlID,$oldUrl,$newUrl));
        }

        if($photo) {

            $sql = <<<QUERY
UPDATE
    review
SET
    title = '$title', description = '$description', class = '$class', score = '$score', photo = '$photo', URL = '$newUrl'
WHERE
    ID = '$id';
QUERY;

            $this->execute($sql);

            $idphoto = rand(0,9999);

            $sql = <<<QUERY
INSERT INTO
    lastphotos
VALUES ('$idphoto','$photo',CURRENT_TIMESTAMP);
QUERY;

            $this->execute($sql);
        }

        else {

            $sql = <<<QUERY
UPDATE review SET
    title='$title',description='$description',class='$class',score='$score',URL='$newUrl'
WHERE
    ID='$id';
QUERY;

            $this->execute($sql);

        }

    }

    // Agafa una review antiga
    public function getOldReview($url) {

    $sql = <<<QUERY
SELECT
    newURL
FROM
    oldurl
WHERE
    oldURL = '$url';
QUERY;


        return $this->getAll($sql)[0]['newURL'];
    }

    public function getBestReview() {

        $sql = <<<QUERY
SELECT
    *
FROM
    review
ORDER BY
    score DESC
LIMIT
    1;
QUERY;


        return $this->getAll($sql)[0];
    }

    public function getLastTenReviews() {

        $sql = <<<QUERY
SELECT
    *
FROM
    review
ORDER BY
    dateCreated
LIMIT
    10;
QUERY;


        return $this->getAll($sql);
    }

    public function getLastTenPhotos() {

        $sql = <<<QUERY
SELECT
    *
FROM
    lastphotos
ORDER BY
    created
LIMIT
    10;
QUERY;


        return $this->getAll($sql);
    }

    public function search($string) {

        $sql = <<<QUERY
SELECT
    *
FROM
    review
WHERE
    title LIKE '%$string%' OR description LIKE '%$string%';
QUERY;

        return $this->getAll($sql);
    }


    public function pointReview($review,$point,$user){

        $sql = <<<QUERY
SELECT
    *
FROM
    vote
WHERE
    review = '$review' AND name = '$user';
QUERY;

        $new = $this->getAll($sql);

        if(empty($new)){

            $sql = <<<QUERY
INSERT INTO
    vote
VALUES ('$user','$review','$point',CURRENT_TIMESTAMP);
QUERY;
            $this->execute($sql);
        }
        else {

            $sql = <<<QUERY
UPDATE
    vote
SET
    npoints = '$point', date=CURRENT_TIMESTAMP
WHERE
    name = '$user' AND review = '$review';
QUERY;

            $this->execute($sql);

        }

        $sql = <<<QUERY
SELECT
    *
FROM
    vote
WHERE
    review = '$review';
QUERY;

        $new = $this->getAll($sql);

        $total = count($new);
        $mean = 0;



        for($i=0;$i<$total;$i++){

            $mean += $new[$i]['npoints'];
        }

        if($mean!=0)
            $mean = $mean/$total;

        echo $mean;
        echo $total;

        $sql = <<<QUERY
UPDATE
    review
SET
    npoints = '$total', meanpoints = '$mean'
WHERE
    ID = '$review';
QUERY;

        $this->execute($sql);


    }

    public function getMyPoint($review,$user) {

        $sql = <<<QUERY
SELECT
    npoints
FROM
    vote
WHERE
    review = '$review' AND name = '$user';
QUERY;

        $new = $this->getAll($sql);

        if(isset($new) && isset($new[0])){
            return $new[0]['npoints'];
        }
        else
            return false;
    }

    public function getMyPoints($user) {

    $sql = <<<QUERY
SELECT
    review.ID,vote.date,vote.npoints,review.URL,review.title
FROM
    vote JOIN review ON vote.review=review.ID
WHERE
    vote.name = '$user';
QUERY;

    $new = $this->getAll($sql);

    if(isset($new) && isset($new[0])){
        return $new;
    }
    else
        return false;
}

    public function deleteVote($review,$user) {

        $sql = <<<QUERY
DELETE FROM
    vote
WHERE
    review = '$review' AND name = '$user' ;
QUERY;

        $this->execute($sql);

        $sql = <<<QUERY
SELECT
    *
FROM
    vote
WHERE
    review = '$review';
QUERY;

        $new = $this->getAll($sql);

        $total = count($new);
        $mean = 0;



        for($i=0;$i<$total;$i++){

            $mean += $new[$i]['npoints'];
        }

        if($mean!=0)
            $mean = $mean/$total;

        echo $mean;
        echo $total;

        $sql = <<<QUERY
UPDATE
    review
SET
    npoints = '$total', meanpoints = '$mean'
WHERE
    ID = '$review';
QUERY;

        $this->execute($sql);
    }

    public function getBestReviews() {

        $sql = <<<QUERY
SELECT
    *
FROM
    review
ORDER BY
    meanpoints DESC
LIMIT
    10;
QUERY;

        $new = $this->getAll($sql);

        if(isset($new) && isset($new[0])){
            return $new;
        }
        else
            return false;
    }

}