# LaSalle Reviews #
## Introducción ##

LaSalle Reviews es una aplicación web que tiene como objetivo fomentar el intercambio de opiniones de cómo se realizan las clases de diferentes asignaturas de la escuela mediante reviews. La idea detrás de la plataforma es que los estudiantes puedan registrarse y postear sus opiniones de clases concretas valorándolas con una puntuación del 1 al 10. Éstas pueden ser después valoradas por otros estudiantes permitiendo así ver un criterio más general. LaSalle Reviews ha sido desarrollada en PHP utilizando Framework Lite. 

![New Review](https://bitbucket-assetroot.s3.amazonaws.com/repository/bgy4dA/2337027321-Screenshot%20from%202014-11-19%2013%3A59%3A18.png?Signature=cROE%2F78tdy5m5P0%2FFW2s8RH2UW4%3D&Expires=1416404023&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82)

## Funciones ##
### Registro ###
Entrando en la web el usuario tiene opción de registrarse si tiene un login válido de la casa. La plataforma está estructurada de manera que si el usuario no está registrado pueda igualmente ver las reviews; aunque no podrá valorarlas. 

Entrando en el formulario de registro se le pedirán al usuario su nombre, su login, su email y una contraseña. Las contraseñas se guardan haciendo un hash md5 de la misma. El nombre y el email pueden introducirse a través del botón ‘Login with Facebook’. Si los campos son  válidos se le enviará al email introducido un link de confirmación. Hasta que no se acceda a ese link la cuenta no quedará validada y por lo tanto el usuario no podrá hacer login.

### Login ###
Una vez registrado el usuario podrá loggearse en la web introduciendo su email y su contraseña o a través del botón ‘login with Facebook’. El menú de navegación entonces cambiará y mostrará opciones para crear reviews, ver las reviews creadas por uno mismo y ver las valoraciones hechas a otras reviews.

![Registro](https://bitbucket-assetroot.s3.amazonaws.com/repository/bgy4dA/606402484-Screenshot%20from%202014-11-19%2014%3A08%3A18.png?Signature=gRljGFvyVPUy6AesVObQMA0%2Bx0w%3D&Expires=1416404317&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82)

### Crear review ###
En esta función el usuario podrá crear una review. Para ello se le pedirá como información un título (el cual tiene que ser único), una descripción, la clase, el día de la clase, una fotografía y una valoración personal de la clase (no de la review). El formulario es validado tanto en el cliente como en el servidor.

![Menu](https://bitbucket-assetroot.s3.amazonaws.com/repository/bgy4dA/2660744586-Screenshot%20from%202014-11-19%2014%3A10%3A00.png?Signature=nUFvIG%2FQLKpAdtq6z7zVXfNgPRs%3D&Expires=1416404427&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82)

### Modificar review ###
El usario registrado también tiene la posibilidad de modificar su review cambiando todos los campos antes comentados. Además si él desea también puede eliminar la review de la plataforma.

### Valorar review ###
Como privilegio final el usuario también tiene la posibilidad de valorar reviews de otros usuarios. Solo podrá valorar una vez, aunque podrá modificar esta valoración las veces que desee.

### Buscar review ###
Desde el header el usuario será capaz de buscar cualquier review a través de su título o de su descripción.

## Estructura ##
La aplicación web sigue el modelo de diseño MVC utilizando Smarty y PHP. Una vez se entra a la página web el dispatcher del framework se encargará de llamar al controlador adecuado en función de la URL.

LaSalle Reviews tiene un controlador para cada vista. Los controladores están diferenciados en cuatro grupos:

 - **Home**: contiene el controlador de home.
 - **Login**: contiene los controladores de login y register además de los scripts PHP necesarios para hacer factibles estas funcionalidades.
 - **Review**: contiene los controladores correspondientes a las reviews encargados de crear, eliminar, modificar y mostrar reviews.
 - **Shared**: contiene los módulos que pertenecen a  otros controladores. Dentro de este grupo están el header el footer y el buscador además de otros módulos para mostrar ciertos tipos de review.

Para cada controlador hay una vista, las carpetas siguen la misma estructura que con los controladores. En el caso de los modelos sólo hay dos, uno para manejar la información correspondiente a los usuarios y otro para manejar la información correspondiente a las reviews.

## Base de datos ##
La base de datos que utiliza LaSalle reviews consta de seis tablas diferentes. Las principales y con información más importante son user, review y vote:

 - **User**: contiene toda la información correspondiente al usuario.
 - **Review**: contiene toda la información correspondiente a las reviews.
 - **Vote**: contiene la información de los votos, sus atributos son name (nombre del usuario), review (identificador de la review) y puntuación. A través de esta tabla es posible calcular el número y la media de cualquier valoración.
 - **Oldurl**: contiene la información de las URLs de las reviews que han sufrido una modificación en su título. Sirve para saber dónde redireccionar al usuario si accede a un link antiguo de una review.
 - **Newuser**: contiene la información del usuario que aún no se ha confirmado a través del link de confirmación.
 - **Lastphotos**: contiene todas las fotografías y las modificaciones hechas a ésta.