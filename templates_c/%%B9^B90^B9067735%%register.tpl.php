<?php /* Smarty version 2.6.14, created on 2014-05-20 17:04:26
         compiled from login/register.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<div id="registerForm">
    <?php if ($this->_tpl_vars['confirmed']): ?>
    <div id="confirmation">
        <h1>Welcome!</h1>
        <p>Welcome <span><?php echo $this->_tpl_vars['inputInfo']['name']; ?>
</span>! LaSalle Reviews is more than happy to see you have joined
            our community. Start posting and rating reviews <span>now</span>!
        </p>
        <br><br>
        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/home">I'm Ready!</a>
        <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/wine.png">
    </div>
    <?php else: ?>
        <?php if (! $this->_tpl_vars['registered']): ?>
        <h1>Join LaSalle Reviews today.</h1>
        <form class="register" method="POST" action="/signup">
            <label for="name">Name:</label>

            <?php if (( $this->_tpl_vars['inputError']['nameEmpty'] || $this->_tpl_vars['inputError']['name'] ) && ! $this->_tpl_vars['facebook']): ?>
                <input class="inputRegister errorCaja" id="nameRegister" value="<?php echo $this->_tpl_vars['inputInfo']['name']; ?>
" name="nameRegister" type="text">
            <?php else: ?>
                <input class="inputRegister" id="nameRegister" value="<?php echo $this->_tpl_vars['inputInfo']['name']; ?>
" name="nameRegister" type="text">
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['nameEmpty']): ?>
                <p class="error">You have to introduce a valid name.</p>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['name']): ?>
                <p class="error">This name is already chosen.</p>
            <?php endif; ?>

            <label for="login">Login:</label>
            <?php if (( $this->_tpl_vars['inputError']['loginEmpty'] || $this->_tpl_vars['inputError']['login'] || $this->_tpl_vars['inputError']['loginNoValid'] ) && ! $this->_tpl_vars['facebook']): ?>
                <input class="inputRegister errorCaja"  id="loginRegister" value="<?php echo $this->_tpl_vars['inputInfo']['login']; ?>
" name="loginRegister" type="text"">
            <?php else: ?>
                <input class="inputRegister"  id="loginRegister" value="<?php echo $this->_tpl_vars['inputInfo']['login']; ?>
" name="loginRegister" type="text"">
            <?php endif; ?>
            <?php if (! $this->_tpl_vars['facebook'] && ( $this->_tpl_vars['inputError']['loginEmpty'] || $this->_tpl_vars['inputError']['loginNoValid'] )): ?>
                <p class="error">You have to introduce a valid login.</p>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['login']): ?>
            <p class="error">This login is already chosen.</p>
            <?php endif; ?>
            <label for="email">E-mail:</label>

            <?php if (! $this->_tpl_vars['facebook'] && ( $this->_tpl_vars['inputError']['emailEmpty'] || $this->_tpl_vars['inputError']['email'] )): ?>
                <input class="inputRegister errorCaja" id="emailRegister" value="<?php echo $this->_tpl_vars['inputInfo']['email']; ?>
" name="emailRegister" type="text"">
            <?php else: ?>
            <input class="inputRegister" id="emailRegister" value="<?php echo $this->_tpl_vars['inputInfo']['email']; ?>
" name="emailRegister" type="text"">
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['emailEmpty']): ?>
                <p class="error">You have to introduce a valid e-mail.</p>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['email']): ?>
            <p class="error">This e-mail is already chosen.</p>
            <?php endif; ?>


            <label for="password">Password:</label>
            <?php if (! $this->_tpl_vars['facebook'] && $this->_tpl_vars['inputError']['password']): ?>
            <input class="inputRegister errorCaja"  id="passwordRegister" value="<?php echo $this->_tpl_vars['inputInfo']['password']; ?>
" name="passwordRegister" type="password"">

            <?php else: ?>
                <input class="inputRegister" id="passwordRegister" value="<?php echo $this->_tpl_vars['inputInfo']['password']; ?>
" name="passwordRegister" type="password"">
            <?php endif; ?>
            <?php if (! $this->_tpl_vars['facebook'] && $this->_tpl_vars['inputError']['password']): ?>
            <p class="error">The password needs to have between 6 and 20 characters.</p>
            <?php endif; ?>
            <input class="inputRegister" type="submit" value="Register" onfocus="inputFocus(this)" onblur="inputBlur(this)">
        </form>
        <?php if (! $this->_tpl_vars['facebook']): ?>
            <a href="javascript:FBRegister();" id="registerFacebook">Register with Facebook</a>
        <?php endif; ?>
        <a href="javascript:FBLogout();" >FB Log Out</a>
        <?php else: ?>
            <div id="confirmation">
                <h1>Awesome!</h1>
                <p>There is only one step more to follow if you want to be part of the LaSalle Reviews community. An
                   activation link has been sent to <span><?php echo $this->_tpl_vars['inputInfo']['email']; ?>
</span>. Use that link to activate your account.
                </p>
                <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/signup/<?php echo $this->_tpl_vars['code']; ?>
">Confirmation</a>
                <p>We will be here waiting for you!</p>
                <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/target.png">
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>



<?php echo $this->_tpl_vars['modules']['footer']; ?>