<?php /* Smarty version 2.6.14, created on 2014-05-21 14:45:13
         compiled from review/review.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<?php if ($this->_tpl_vars['review']): ?>
        <div id="reviewBox" class="reviewBig">
        <h1 class="break" id="title"><?php echo $this->_tpl_vars['review']['title']; ?>
</h1>
        <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/large/<?php echo $this->_tpl_vars['review']['photo']; ?>
">
        <p class="break"><label>Class: </label><?php echo $this->_tpl_vars['review']['class']; ?>
</p>
        <p class="break"><label>Date: </label><?php echo $this->_tpl_vars['review']['date']; ?>
</p>
        <p class="break"><label>Description: </label><?php echo $this->_tpl_vars['review']['description']; ?>
</p>
        <p class="break"><label>Author: </label><?php echo $this->_tpl_vars['review']['author']; ?>
</p>
        <p class="break"><label>Login: </label><?php echo $this->_tpl_vars['review']['login']; ?>
</p>
        <p><label>Score: </label><?php echo $this->_tpl_vars['review']['score']; ?>
</p>
        <p><label>Published: </label><?php echo $this->_tpl_vars['review']['dateCreated']; ?>
</p>
        </div>
        <?php echo $this->_tpl_vars['modules']['point']; ?>

<?php echo $this->_tpl_vars['modules']['bestreviews']; ?>



<?php else: ?>
    <?php $_from = $this->_tpl_vars['reviewArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reviews']):
?>
    <div id="reviewBox">
        <h1 class="break" id="title"><?php echo $this->_tpl_vars['reviews']['title']; ?>
</h1>
        <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/small/<?php echo $this->_tpl_vars['reviews']['photo']; ?>
">
        <p class="break"><label>Class: </label><?php echo $this->_tpl_vars['reviews']['class']; ?>
</p>
        <p class="break"><label>Date: </label><?php echo $this->_tpl_vars['reviews']['date']; ?>
</p>
        <p class="break"><label>Description: </label><?php echo $this->_tpl_vars['reviews']['description']; ?>
</p>
        <p class="break"><label>Author: </label><?php echo $this->_tpl_vars['reviews']['author']; ?>
</p>
        <p class="break"><label>Login: </label><?php echo $this->_tpl_vars['reviews']['login']; ?>
</p>
        <p><label>Score: </label><?php echo $this->_tpl_vars['reviews']['score']; ?>
</p>
        <p><label>Published: </label><?php echo $this->_tpl_vars['reviews']['dateCreated']; ?>
</p>
        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r/<?php echo $this->_tpl_vars['reviews']['URL']; ?>
">Go to review!</a>
    </div>
    <?php endforeach; endif; unset($_from); ?>
    <div id="nextPrev">
    <?php if ($this->_tpl_vars['previous'] >= 0): ?>
        <a id="nextPrev" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r/<?php echo $this->_tpl_vars['previous']; ?>
">Previous</a>
    <?php endif; ?>
    <?php if ($this->_tpl_vars['page'] < $this->_tpl_vars['totalPages']): ?>
        <a id="nextPrev" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r/<?php echo $this->_tpl_vars['page']; ?>
">Next</a>
    <?php endif; ?>
    </div>
<?php endif; ?>
<?php echo $this->_tpl_vars['modules']['footer']; ?>