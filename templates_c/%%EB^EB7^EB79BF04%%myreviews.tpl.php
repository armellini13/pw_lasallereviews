<?php /* Smarty version 2.6.14, created on 2014-05-21 10:23:58
         compiled from review/myreviews.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'is_array', 'review/myreviews.tpl', 66, false),)), $this); ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<?php if ($this->_tpl_vars['edit']): ?>
<div id="newForm">
    <h1>Edit Review</h1>
    <form class="newReview" method="POST" enctype="multipart/form-data" name="newReview">
        <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['inputInfo']['ID']; ?>
">
        <input type="hidden" name="edit" value="<?php echo $this->_tpl_vars['edit']; ?>
">
        <label for="titleReview">Title:</label>
        <?php if (( $this->_tpl_vars['inputError']['title'] || $this->_tpl_vars['inputError']['titleUnique'] || $this->_tpl_vars['inputError']['titleEmpty'] )): ?>
        <input class="inputRegister errorCaja" value="<?php echo $this->_tpl_vars['inputInfo']['title']; ?>
" name="titleReview" type="text">
        <?php else: ?>
        <input class="inputRegister" value="<?php echo $this->_tpl_vars['inputInfo']['title']; ?>
" name="titleReview" type="text">
        <?php endif; ?>
        <?php if ($this->_tpl_vars['inputError']['title']): ?>
        <p class="error">You have to introduce a valid title.</p>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['inputError']['titleEmpty']): ?>
        <p class="error">You have to introduce a title.</p>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['inputError']['titleUnique']): ?>
        <p class="error">This title already exists.</p>
        <?php endif; ?>

        <label for="descriptionReview">Description:</label>
        <textarea class="inputRegister" name="descriptionReview" id="descriptionReview" type="text"">
        <?php echo $this->_tpl_vars['inputInfo']['description']; ?>

        </textarea>
        <br>
        <?php if ($this->_tpl_vars['inputError']['description']): ?>
        <p class="error">You have to introduce a description.</p>
        <?php endif; ?>
        <label for="classReview">Class:</label>
        <?php if ($this->_tpl_vars['inputError']['class']): ?>
        <input class="inputRegister errorCaja" value="<?php echo $this->_tpl_vars['inputInfo']['class']; ?>
" name="classReview" type="text"">
        <?php else: ?>
        <input class="inputRegister" value="<?php echo $this->_tpl_vars['inputInfo']['class']; ?>
" name="classReview" type="text"">
        <?php endif; ?>
        <?php if ($this->_tpl_vars['inputError']['class']): ?>
        <p class="error">You have to introduce a class.</p>
        <?php endif; ?>
        <label for="dateReview">Date:</label>
        <?php if ($this->_tpl_vars['inputError']['date']): ?>
        <input class="inputRegister errorCaja" value="<?php echo $this->_tpl_vars['inputInfo']['date']; ?>
" type="text" id="datepicker" name="dateReview"/>
        <?php else: ?>
        <input class="inputRegister" value="<?php echo $this->_tpl_vars['inputInfo']['date']; ?>
" type="text" id="datepicker" name="dateReview"/>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['inputError']['date']): ?>
        <p class="error">You have to introduce a valid date.</p>
        <?php endif; ?>
        <label for="scoreReview">Score:</label>
        <select value="<?php echo $this->_tpl_vars['inputInfo']['score']; ?>
" class="inputRegister"  id="otherInput" name="scoreReview">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
        </select>
        <input class="inputRegister" id="photoInput" type="hidden" name="savedInTemporal" value="<?php echo $this->_tpl_vars['inputInfo']['savedInTemporal']; ?>
"/>

        <?php if (! ((is_array($_tmp=$this->_tpl_vars['inputInfo']['photo'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp))): ?>
            <input class="inputRegister" id="PUTA" type="hidden" name="oldPhoto" value="<?php echo $this->_tpl_vars['inputInfo']['photo']; ?>
"/>
            <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/small/<?php echo $this->_tpl_vars['inputInfo']['photo']; ?>
" id="tempPhoto">
        <?php else: ?>
            <input class="inputRegister" id="photoInput" type="hidden" name="oldPhoto" value="<?php echo $this->_tpl_vars['inputInfo']['oldPhoto']; ?>
"/>
            <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/small/<?php echo $this->_tpl_vars['inputInfo']['oldPhoto']; ?>
" id="tempPhoto">
        <?php endif; ?>
        <br>
        <label for="photoReview">Photo:</label>
        <input class="inputRegister" id="photoInput" type="file" name="photoReview" value="<?php echo $this->_tpl_vars['inputInfo']['photo']['tmp_name']; ?>
"/>
        <?php if ($this->_tpl_vars['inputError']['photoEmpty']): ?>
        <p class="error">You have to introduce a photo.</p>
        <?php else: ?>
        <?php if ($this->_tpl_vars['inputError']['photo']): ?>
        <p class="error">Your photo has to be .gif, .jpeg or .png to be uploaded.</p>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['inputError']['photoSize']): ?>
        <p class="error">Your photo is too big to be uploaded.</p>
        <?php endif; ?>
        <?php endif; ?>
        <input class="inputRegister" type="submit" value="Upload Review" onfocus="inputFocus(this)" onblur="inputBlur(this)">
    </form>
</div>
<?php else: ?>
    <?php $_from = $this->_tpl_vars['reviewArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reviews']):
?>
    <div id="reviewBox">
        <div id="delete">
            <h1 class="break" id="title"><?php echo $this->_tpl_vars['reviews']['title']; ?>
</h1>
            <form method="POST" onsubmit="return confirm('Do you really want to delete the review?');">
                <input type="hidden" name="IDdelete" value="<?php echo $this->_tpl_vars['reviews']['ID']; ?>
">
                <input type="hidden" name="delete" value="true">
                <input type="submit" value="Delete">
            </form>
            <form method="POST" id="edit">
                <input type="hidden" name="IDedit" value="<?php echo $this->_tpl_vars['reviews']['ID']; ?>
">
                <input type="hidden" name="edit" value="true">
                <input type="submit" value="Edit">
            </form>

        </div>
        <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/small/<?php echo $this->_tpl_vars['reviews']['photo']; ?>
">
        <p class="break"><label>Class: </label><?php echo $this->_tpl_vars['reviews']['class']; ?>
</p>
        <p class="break"><label>Date: </label><?php echo $this->_tpl_vars['reviews']['date']; ?>
</p>
        <p class="break"><label>Description: </label><?php echo $this->_tpl_vars['reviews']['description']; ?>
</p>
        <p class="break"><label>Author: </label><?php echo $this->_tpl_vars['reviews']['author']; ?>
</p>
        <p class="break"><label>Login: </label><?php echo $this->_tpl_vars['reviews']['login']; ?>
</p>
        <p><label>Score: </label><?php echo $this->_tpl_vars['reviews']['score']; ?>
</p>
        <p><label>Published: </label><?php echo $this->_tpl_vars['reviews']['dateCreated']; ?>
</p>
        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r/<?php echo $this->_tpl_vars['reviews']['URL']; ?>
">Go to review!</a>
    </div>
    <?php endforeach; endif; unset($_from); ?>
    <div id="nextPrev">
        <?php if ($this->_tpl_vars['previous'] >= 0): ?>
        <a id="nextPrev" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/myreviews/<?php echo $this->_tpl_vars['previous']; ?>
">Previous</a>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['page'] < $this->_tpl_vars['totalPages']): ?>
        <a id="nextPrev" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/myreviews/<?php echo $this->_tpl_vars['page']; ?>
">Next</a>
        <?php endif; ?>
    </div>
<?php endif; ?>
<?php echo $this->_tpl_vars['modules']['footer']; ?>