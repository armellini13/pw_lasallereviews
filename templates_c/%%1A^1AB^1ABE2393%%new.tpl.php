<?php /* Smarty version 2.6.14, created on 2014-05-21 08:57:21
         compiled from review/new.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

<script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

<div id="newForm">
    <?php if ($this->_tpl_vars['uploaded']): ?>
        <h1>Review succesfully uploaded!</h1>
        <p>Follow this link in order to see your review!</p>
        <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/box.png">
    <?php else: ?>
        <h1>New review</h1>
        <form class="newReview" method="POST" enctype="multipart/form-data" name="newReview">
            <label for="titleReview">Title:</label>
            <?php if (( $this->_tpl_vars['inputError']['title'] || $this->_tpl_vars['inputError']['titleUnique'] || $this->_tpl_vars['inputError']['titleEmpty'] )): ?>
                <input class="inputRegister errorCaja" value="<?php echo $this->_tpl_vars['inputInfo']['title']; ?>
" name="titleReview" type="text">
            <?php else: ?>
                <input class="inputRegister" value="<?php echo $this->_tpl_vars['inputInfo']['title']; ?>
" name="titleReview" type="text">
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['title']): ?>
                <p class="error">You have to introduce a valid title.</p>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['titleEmpty']): ?>
                <p class="error">You have to introduce a title.</p>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['titleUnique']): ?>
                <p class="error">This title already exists.</p>
            <?php endif; ?>

            <label for="descriptionReview">Description:</label>
            <textarea class="inputRegister" name="descriptionReview" id="descriptionReview" type="text"">
            <?php echo $this->_tpl_vars['inputInfo']['description']; ?>

            </textarea>
            <br>
            <?php if ($this->_tpl_vars['inputError']['description']): ?>
                <p class="error">You have to introduce a description.</p>
            <?php endif; ?>
            <label for="classReview">Class:</label>
            <?php if ($this->_tpl_vars['inputError']['class']): ?>
            <input class="inputRegister errorCaja" value="<?php echo $this->_tpl_vars['inputInfo']['class']; ?>
" name="classReview" type="text"">
            <?php else: ?>
            <input class="inputRegister" value="<?php echo $this->_tpl_vars['inputInfo']['class']; ?>
" name="classReview" type="text"">
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['class']): ?>
                <p class="error">You have to introduce a class.</p>
            <?php endif; ?>
            <label for="dateReview">Date:</label>
            <?php if ($this->_tpl_vars['inputError']['date']): ?>
                <input class="inputRegister errorCaja" value="<?php echo $this->_tpl_vars['inputInfo']['date']; ?>
" type="text" id="datepicker" name="dateReview"/>
            <?php else: ?>
                <input class="inputRegister" value="<?php echo $this->_tpl_vars['inputInfo']['date']; ?>
" type="text" id="datepicker" name="dateReview"/>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['inputError']['date']): ?>
                <p class="error">You have to introduce a valid date.</p>
            <?php endif; ?>
            <label for="scoreReview">Score:</label>
            <select value="<?php echo $this->_tpl_vars['inputInfo']['score']; ?>
" class="inputRegister"  id="otherInput" name="scoreReview">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
            <input class="inputRegister" id="photoInput" type="hidden" name="savedInTemporal" value="<?php echo $this->_tpl_vars['inputInfo']['savedInTemporal']; ?>
"/>
            <input class="inputRegister" id="photoInput" type="hidden" name="tempPhotoName" value="<?php echo $this->_tpl_vars['inputInfo']['tempPhotoName']; ?>
"/>
            <input class="inputRegister" id="photoInput" type="hidden" name="tempPhotoPath" value="<?php echo $this->_tpl_vars['inputInfo']['tempPhotoPath']; ?>
"/>
            <?php if ($this->_tpl_vars['inputInfo']['savedInTemporal']): ?>
            <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/tmp/<?php echo $this->_tpl_vars['inputInfo']['tempPhotoName']; ?>
" id="tempPhoto">
            <?php else: ?>
                <label for="photoReview">Photo:</label>
                <input class="inputRegister" id="photoInput" type="file" name="photoReview" value="<?php echo $this->_tpl_vars['inputInfo']['photo']['tmp_name']; ?>
"/>
            <?php if ($this->_tpl_vars['inputError']['photoEmpty']): ?>
                    <p class="error">You have to introduce a photo.</p>
                <?php else: ?>
                    <?php if ($this->_tpl_vars['inputError']['photo']): ?>
                        <p class="error">Your photo has to be .gif, .jpeg or .png to be uploaded.</p>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['inputError']['photoSize']): ?>
                        <p class="error">Your photo is too big to be uploaded.</p>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
            <input class="inputRegister" type="submit" value="Upload Review" onfocus="inputFocus(this)" onblur="inputBlur(this)">
        </form>
    <?php endif; ?>
</div>



<?php echo $this->_tpl_vars['modules']['footer']; ?>