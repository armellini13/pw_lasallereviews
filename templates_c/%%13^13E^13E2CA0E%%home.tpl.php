<?php /* Smarty version 2.6.14, created on 2014-05-21 14:42:47
         compiled from home/home.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'home/home.tpl', 25, false),)), $this); ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

<div id="reviewBox">
<h1>Best Review</h1>
</div>
<div id="reviewBox" class="bestReview">
    <a id="nolink" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r/<?php echo $this->_tpl_vars['bestReview']['URL']; ?>
"> <h1 class="break" id="title"><?php echo $this->_tpl_vars['bestReview']['title']; ?>
</h1></a>
    <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/small/<?php echo $this->_tpl_vars['bestReview']['photo']; ?>
">
    <p class="break"><label>Class: </label><?php echo $this->_tpl_vars['bestReview']['class']; ?>
</p>
    <p class="break"><label>Date: </label><?php echo $this->_tpl_vars['bestReview']['date']; ?>
</p>
    <p class="break"><label>Description: </label><?php echo $this->_tpl_vars['bestReview']['description']; ?>
</p>
    <p class="break"><label>Author: </label><?php echo $this->_tpl_vars['bestReview']['author']; ?>
</p>
    <p class="break"><label>Login: </label><?php echo $this->_tpl_vars['bestReview']['login']; ?>
</p>
    <p><label>Score: </label><?php echo $this->_tpl_vars['bestReview']['score']; ?>
</p>
    <p><label>Published: </label><?php echo $this->_tpl_vars['bestReview']['dateCreated']; ?>
</p>
</div>
    <a class="twitter-timeline" href="https://twitter.com/search?q=%23projectesWeb14" data-widget-id="462055791529177089">Tweets sobre "#projectesWeb14"</a>

<div id="reviewBox">
    <h1>Last 10 Reviews</h1>
</div>
<?php $_from = $this->_tpl_vars['lastTen']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reviews']):
?>
<div id="reviewBox" class="bg">
    <a id="nolink" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r/<?php echo $this->_tpl_vars['reviews']['URL']; ?>
"><h1 class="break" id="title"><?php echo $this->_tpl_vars['reviews']['title']; ?>
</h1></a>
    <p class="break"><label>Date: </label><?php echo $this->_tpl_vars['reviews']['date']; ?>
</p>
    <p class="break"><label>Description: </label><?php echo ((is_array($_tmp=$this->_tpl_vars['reviews']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)); ?>
</p>
    <p><label>Score: </label><?php echo $this->_tpl_vars['reviews']['score']; ?>
</p>
</div>
<?php endforeach; endif; unset($_from); ?>

<div id="reviewBox">
    <h1>Last 10 Photos Uploaded</h1>
</div>
<div id="reviewBox">
<?php $_from = $this->_tpl_vars['lastPhotos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['photos']):
?>
    <img id="lastTen" src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/small/<?php echo $this->_tpl_vars['photos']['photo']; ?>
">
<?php endforeach; endif; unset($_from); ?>
</div>
<?php echo $this->_tpl_vars['modules']['bestreviews']; ?>

<?php echo $this->_tpl_vars['modules']['footer']; ?>