<?php /* Smarty version 2.6.14, created on 2014-05-21 14:26:35
         compiled from review/myvotes.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<?php if ($this->_tpl_vars['votes']): ?>
    <?php $_from = $this->_tpl_vars['myVotes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reviews']):
?>
    <div id="reviewBox" class="bg">
        <a id="nolink" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r/<?php echo $this->_tpl_vars['reviews']['URL']; ?>
"><h1 class="break" id="title"><?php echo $this->_tpl_vars['reviews']['title']; ?>
</h1></a>
        <form method="POST" onsubmit="return confirm('Do you really want to delete the review?');">
            <input type="hidden" name="IDdelete" value="<?php echo $this->_tpl_vars['reviews']['ID']; ?>
">
            <input type="hidden" name="delete" value="true">
            <input type="submit" value="Delete">
        </form>
        <p class="break"><label>Date: </label><?php echo $this->_tpl_vars['reviews']['date']; ?>
</p>
        <p class="break"><label>Vote: </label><?php echo $this->_tpl_vars['reviews']['npoints']; ?>
</p>
    </div>
    <?php endforeach; endif; unset($_from); ?>
<?php else: ?>
<div id="reviewBox" class="bg">
    <h1 class="break" id="title">You still haven't voted</h1>
</div>
<?php endif; ?>

<?php echo $this->_tpl_vars['modules']['footer']; ?>