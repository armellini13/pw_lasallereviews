<?php /* Smarty version 2.6.14, created on 2014-05-21 13:59:37
         compiled from shared/head.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'shared/head.tpl', 37, false),)), $this); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="La Salle Reviews" />
	<meta name="keywords" content="laSalle, reviews, university, teachers, students, opinion" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />		
	<title>LaSalle Reviews</title>
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/css/style.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <link rel="shortcut icon" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/logo.ico">
    <script type="text/javascript" id="facebook-jssdk" src="http://connect.facebook.net/en_US/all.js"></script>
    <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/fb.js"></script>

</head>
<body>


    <header>
        <div class="logo">
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
">
                <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/logo.png">
                <h1>LaSalle Reviews</h1>
            </a>
        </div>
        <div class="searchBox">
            <form method="POST" action="/search">
                <input id="box" name="search" type="text" value="Comment, title.." onfocus="inputFocus(this)" onblur="inputBlur(this)">
                <input id="submit" type="submit" value="Search">
            </form>
        </div>
        <?php if ($this->_tpl_vars['logged']): ?>
        <div class="logged">
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/myreviews" id="user" id="login" class="options"><?php echo ((is_array($_tmp=$this->_tpl_vars['user'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20) : smarty_modifier_truncate($_tmp, 20)); ?>
</a>
            <form method="POST" action="/login">
                <input type="hidden" name="logout" value="true">
                <input id="logout" type="submit" value="Log out">
            </form>
        </div>
        <?php else: ?>
            <div class="login">
                <a href="#" id="login" class="options">Log in</a>
                <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/signup" id="signUp" class="options">Sign up</a>
                <div class="arrow"></div>
                <div class="boxLogin">
                    <form id="menuLogin" method="POST" action="/login">
                        <label for="box">User:</label> <input id="box" type="text" name="email" value="email" onfocus="inputFocus(this)" onblur="inputBlur(this)">
                        <label for="box">Password:</label>  <input id="box" type="password" name="password" value="password" onfocus="inputFocus(this)" onblur="inputBlur(this)">
                        <input id="submit" type="submit" value="Log In">
                    </form>
                    <form method="POST" action="/login">
                        <input type="hidden" name="facebook" value="true">
                        <input id="facebook" type="submit" value="Log in with Facebook">
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </header>
    <header id="secondHeader">
        <div id="secondLogo">
            <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/logo.png">
        </div>
        <ul id="secondMenu">
            <?php if ($this->_tpl_vars['logged']): ?>
                <?php if ($this->_tpl_vars['header'][0] == 'n'): ?>
                    <li class="active"><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/new">+New</a></li>
                <?php else: ?>
                    <li><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/new" >+New</a></li>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['header'][0] == 'h' || $this->_tpl_vars['header'][0] == 'd'): ?>
                <li class="active"><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
">Home</a></li>
            <?php else: ?>
                <li><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
">Home</a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['header'][0] == 'r'): ?>
                <li class="active"><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r"">Reviews</a></li>
            <?php else: ?>
                <li><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/r">Reviews</a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['logged']): ?>
                <?php if ($this->_tpl_vars['header'][0] == 'm'): ?>
                <li class="active"><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/myreviews">My Reviews</a></li>
                <?php else: ?>
                <li><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/myreviews">My Reviews</a></li>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['logged']): ?>
                <?php if ($this->_tpl_vars['header'][0] == 'v'): ?>
                <li class="active"><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/votes">My Votes</a></li>
                <?php else: ?>
                <li><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/votes">My Votes</a></li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
    </header>

	<div id="wrapper">