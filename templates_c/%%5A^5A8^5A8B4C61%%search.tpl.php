<?php /* Smarty version 2.6.14, created on 2014-05-21 14:45:22
         compiled from shared/search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'shared/search.tpl', 8, false),)), $this); ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

<?php $_from = $this->_tpl_vars['results']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['foo'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['foo']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['reviews']):
        $this->_foreach['foo']['iteration']++;
?>
<?php if (( ($this->_foreach['foo']['iteration']-1) >= $this->_tpl_vars['previous'] ) && ( ($this->_foreach['foo']['iteration']-1) < $this->_tpl_vars['next'] )): ?>
<div id="reviewBox">
    <h1 class="break" id="title"><?php echo $this->_tpl_vars['reviews']['title']; ?>
</h1>
    <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/uploads/small/<?php echo $this->_tpl_vars['reviews']['photo']; ?>
">
    <p class="break"><label>Date: </label><?php echo $this->_tpl_vars['reviews']['date']; ?>
</p>
    <p class="break"><label>Description: </label><?php echo ((is_array($_tmp=$this->_tpl_vars['reviews']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)); ?>
</p>
    <p class="break"><label>Login: </label><?php echo $this->_tpl_vars['reviews']['login']; ?>
</p>
    <p><label>Score: </label><?php echo $this->_tpl_vars['reviews']['score']; ?>
</p>
</div>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
<?php if (( $this->_tpl_vars['match'] != 0 ) && $this->_tpl_vars['previous'] != 0 || $this->_tpl_vars['next'] != 0): ?>
<div id="nextPrev">
    <?php if ($this->_tpl_vars['previous'] != 0): ?>
    <form method="POST">
        <input type="hidden" name="previous" value="<?php echo $this->_tpl_vars['previous']; ?>
">
        <input type="hidden" name="search" value="<?php echo $this->_tpl_vars['search']; ?>
">
        <input type="hidden" name="next" value="<?php echo $this->_tpl_vars['next']; ?>
">
        <input type="hidden" name="side" value="previous">
        <input id="submit" type="submit" value="Previous">
    </form>
    <?php endif; ?>
    <?php if ($this->_tpl_vars['next'] != 0 && $this->_tpl_vars['next'] < $this->_tpl_vars['match']): ?>
    <form method="POST">
        <input type="hidden" name="previous" value="<?php echo $this->_tpl_vars['previous']; ?>
">
        <input type="hidden" name="search" value="<?php echo $this->_tpl_vars['search']; ?>
">
        <input type="hidden" name="next" value="<?php echo $this->_tpl_vars['next']; ?>
">
        <input type="hidden" name="side" value="next">
        <input id="submit" type="submit" value="Next">
    </form>
    <?php endif; ?>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['match'] == 0 && ! $this->_tpl_vars['no']): ?>
<div id="reviewBox">
    <h1>No results</h1>
</div>
<?php endif; ?>
<?php echo $this->_tpl_vars['modules']['bestreviews']; ?>



<?php echo $this->_tpl_vars['modules']['footer']; ?>