<?php /* Smarty version 2.6.14, created on 2014-05-21 13:50:30
         compiled from shared/point.tpl */ ?>
<?php if ($this->_tpl_vars['review']): ?>

    <div id="registerForm">
    <?php if ($this->_tpl_vars['registered']): ?>
         <?php if ($this->_tpl_vars['reviewInfo']['npoints'] == 0): ?>
            <p>Nobody has voted this review yet.</p>
        <?php else: ?>
            <p>Points: <?php echo $this->_tpl_vars['reviewInfo']['meanpoints']; ?>
</p>
            <p>Users who voted: <?php echo $this->_tpl_vars['reviewInfo']['npoints']; ?>
</p>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['myPoint']): ?>
            <p>My vote: <?php echo $this->_tpl_vars['myPoint']; ?>
</p>
        <?php else: ?>
            <p>You still haven't voted this review.</p>

        <?php endif; ?>

        <br>
        <form class="newReview" method="POST" enctype="multipart/form-data" name="newReview">
                <select value="<?php echo $this->_tpl_vars['inputInfo']['score']; ?>
" class="inputRegister"  id="otherInput" name="pointReview">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <input class="inputRegister" type="submit" value="Submit" onfocus="inputFocus(this)" onblur="inputBlur(this)">
            </form>
    <?php else: ?>
        <p>You must be logged in to value reviews</p>
        <br>
        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/login" id="facebook">Log In</a>
    <?php endif; ?>
    </div>
<?php endif; ?>
