<?php /* Smarty version 2.6.14, created on 2014-05-20 20:07:09
         compiled from login/login.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<div id="loginForm">

    <?php if ($this->_tpl_vars['logged']): ?>
        <h1>Wait!</h1>
        <p>Seems to be <?php echo $this->_tpl_vars['user']; ?>
 is already logged in! Do you want to log out?</p>
        <form class="login" method="POST" id="logout">
            <input type="hidden" value="true" name="logout2">
            <input type="submit" value="Log Out">
        </form>
        <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/imag/user.png">
    <?php else: ?>
        <h1>Log In</h1>
        <?php if ($this->_tpl_vars['errorLog']): ?>
            <p id="errorMessage">Woops! Can't log in. Check your email or your password..</p>
        <?php endif; ?>
        <form class="login" method="POST">
            <label for="email">E-mail:</label>
            <input value="<?php echo $this->_tpl_vars['inputInfo']['email']; ?>
" name="email" type="text"">
            <label for="password">Password:</label>
            <input value="<?php echo $this->_tpl_vars['inputInfo']['password']; ?>
" name="password" type="password"">
            <input type="submit" name="submitLog" value="Log In">
        </form>
    <?php endif; ?>
</div>

<?php echo $this->_tpl_vars['modules']['footer']; ?>