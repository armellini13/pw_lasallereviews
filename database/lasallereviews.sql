-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2014 a las 14:51:23
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `lasallereviews`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lastphotos`
--

CREATE TABLE IF NOT EXISTS `lastphotos` (
  `ID` int(11) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lastphotos`
--

INSERT INTO `lastphotos` (`ID`, `photo`, `created`) VALUES
(17, '125000.jpg', '2014-05-21 09:54:08'),
(724, '972839.jpg', '2014-05-21 10:15:19'),
(1046, '138702.jpg', '2014-05-21 09:53:34'),
(1339, '919647.jpg', '2014-05-21 09:50:39'),
(1672, '290222.jpg', '2014-05-21 08:45:02'),
(1726, '796722.jpg', '2014-05-21 09:52:42'),
(2136, '117340.jpg', '2014-05-21 10:14:39'),
(3292, '646209.jpg', '2014-05-21 09:51:25'),
(3825, '413757.jpg', '2014-05-21 10:14:12'),
(4221, '300445.jpg', '2014-05-21 10:15:44'),
(4371, '98175.jpg', '2014-05-21 08:36:13'),
(6080, '769897.jpg', '2014-05-21 10:15:04'),
(6670, '595123.jpg', '2014-05-21 10:15:56'),
(7081, '915740.jpg', '2014-05-21 10:15:32'),
(7167, '197357.jpg', '2014-05-21 10:14:26'),
(7262, '293487.jpg', '2014-05-21 09:52:53'),
(7601, '283477.jpg', '2014-05-21 09:51:32'),
(7870, '459899.jpg', '2014-05-21 09:53:08'),
(8418, '339416.jpg', '2014-05-21 10:14:51'),
(9665, '735198.jpg', '2014-05-21 09:51:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newuser`
--

CREATE TABLE IF NOT EXISTS `newuser` (
  `name` varchar(30) NOT NULL,
  `login` varchar(7) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL,
  `code` int(4) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oldurl`
--

CREATE TABLE IF NOT EXISTS `oldurl` (
  `ID` int(11) NOT NULL,
  `oldURL` varchar(250) NOT NULL,
  `newURL` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `ID` int(11) NOT NULL,
  `author` varchar(30) NOT NULL,
  `login` varchar(7) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `class` varchar(40) NOT NULL,
  `date` varchar(10) NOT NULL,
  `score` int(11) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `URL` varchar(250) NOT NULL,
  `npoints` int(11) NOT NULL,
  `meanpoints` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `review`
--

INSERT INTO `review` (`ID`, `author`, `login`, `title`, `description`, `class`, `date`, `score`, `photo`, `dateCreated`, `URL`, `npoints`, `meanpoints`) VALUES
(98175, 'Agustin Armellini Fischer', 'ls25735', 'asdfsadf', 'asdfasdf', 'asdfsd', '19/05/2014', 1, '98175.jpg', '2014-05-21 08:36:13', 'asdfsadf', 0, 0),
(117340, 'Agustin Armellini Fischer', 'ls25735', 'adsfsadffd', 'asdf', 'asdfasdf', '26/05/2014', 1, '117340.jpg', '2014-05-21 10:14:39', 'adsfsadffd', 1, 9),
(125000, 'Agustin Armellini Fischer', 'ls25735', 'asdfdsfdfsa', 'asdf', 'asdf', '28/05/2014', 1, '125000.jpg', '2014-05-21 09:54:08', 'asdfdsfdfsa', 0, 0),
(138702, 'Agustin Armellini Fischer', 'ls25735', 'asdfafd', 'asdfasdf', 'adsf', '27/05/2014', 10, '138702.jpg', '2014-05-21 09:53:34', 'asdfafd', 0, 0),
(197357, 'Agustin Armellini Fischer', 'ls25735', 'asdfasd', 'adsfsadf', 'asdfadf', '19/05/2014', 1, '197357.jpg', '2014-05-21 10:14:26', 'asdfasd', 0, 0),
(283477, 'Agustin Armellini Fischer', 'ls25735', 'asdfaaasaaa', 'aaa', 'aa', '26/05/2014', 1, '283477.jpg', '2014-05-21 09:51:32', 'asdfaaasaaa', 0, 0),
(290222, 'Agustin Armellini Fischer', 'ls25735', 'heeeey', 'asdfasdfsd', 'asdfsadf', '22/05/2014', 10, '290222.jpg', '2014-05-21 08:45:02', 'heeeey', 0, 0),
(293487, 'Agustin Armellini Fischer', 'ls25735', 'asdfaaa', 'asdf', 'afsd', '27/05/2014', 1, '293487.jpg', '2014-05-21 09:52:53', 'asdfaaa', 0, 0),
(300445, 'Agustin Armellini Fischer', 'ls25735', 'adsfadsf', 'asdfdsa', 'fsdf', '30/05/2014', 1, '300445.jpg', '2014-05-21 10:15:44', 'adsfadsf', 0, 0),
(339416, 'Agustin Armellini Fischer', 'ls25735', 'aaaaa', 'sdfa', 'sdfsfd', '29/05/2014', 1, '339416.jpg', '2014-05-21 10:14:51', 'aaaaa', 0, 0),
(413757, 'Agustin Armellini Fischer', 'ls25735', 'asdffffffff', 'asdf', 'a', '27/05/2014', 1, '413757.jpg', '2014-05-21 10:14:11', 'asdffffffff', 0, 0),
(459899, 'Agustin Armellini Fischer', 'ls25735', 'asdfasdfs', 'adsff', 'asdf', '19/05/2014', 1, '459899.jpg', '2014-05-21 09:53:08', 'asdfasdfs', 0, 0),
(595123, 'Agustin Armellini Fischer', 'ls25735', 'asdff', 'asfd', 'sdf', '29/05/2014', 1, '595123.jpg', '2014-05-21 10:15:56', 'asdff', 0, 0),
(646209, 'Agustin Armellini Fischer', 'ls25735', 'asdfaaasa', 'aaa', 'aa', '26/05/2014', 1, '646209.jpg', '2014-05-21 09:51:25', 'asdfaaasa', 0, 0),
(735198, 'Agustin Armellini Fischer', 'ls25735', 'asdfasa', 'aaa', 'aa', '26/05/2014', 9, '735198.jpg', '2014-05-21 09:51:07', 'asdfasa', 0, 0),
(769897, 'Agustin Armellini Fischer', 'ls25735', 'aasdf', 'asdfsdf', 'fdsfds', '22/05/2014', 1, '769897.jpg', '2014-05-21 10:15:04', 'aasdf', 0, 0),
(796722, 'Agustin Armellini Fischer', 'ls25735', 'afdasdf', 'adf', 'adf', '23/05/2014', 10, '796722.jpg', '2014-05-21 09:52:42', 'afdasdf', 0, 0),
(915740, 'Agustin Armellini Fischer', 'ls25735', 'fsdfsdfsdfsa', 'asdfsadf', 'adfds', '27/05/2014', 1, '915740.jpg', '2014-05-21 10:15:32', 'fsdfsdfsdfsa', 0, 0),
(919647, 'Agustin Armellini Fischer', 'ls25735', 'asdfffff', 'asdffff', 'sadf', '27/05/2014', 8, '919647.jpg', '2014-05-21 09:50:39', 'asdfffff', 0, 0),
(972839, 'Agustin Armellini Fischer', 'ls25735', 'fdsdsa', 'aafsdf', 'adffds', '30/05/2014', 10, '972839.jpg', '2014-05-21 10:15:19', 'fdsdsa', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `name` varchar(30) NOT NULL,
  `login` varchar(7) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`name`, `login`, `email`, `password`) VALUES
('Agustin Armellini aFischer', 'ls12322', 'aarmellinifischer01@manhattan.edu', '467e52c6fcf895ddb55f410b2b09f962'),
('asdfasf', 'ls01232', 'ar@msn.com', 'ff3e4b618b07a1f9b2ab04c201ae6613'),
('agg', 'ls09123', 'ara@msn.com', '039fee945400111b8c47bbc332b01d3f'),
('Agustin Armellini Fischer', 'ls25735', 'armellini13@gmail.com', '039fee945400111b8c47bbc332b01d3f'),
('Agustín', 'as12345', 'armellini1a3@gmail.com', '6168f305888c3d795e67c6de17bf8a21'),
('asdfa', 'ls91131', 'asdasdffadsf@msn.com', 'e708864855f3bb69c4d9a213b9108b9f'),
('asdf', 'fd23123', 'asdfadsf@msn.com', 'fe4c5295f954ecc939a2a6900bbaaab1'),
('Vector', 'ls34543', 'victourg@gmail.com', '5985166d1051f1d51dbe9024231271ae');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vote`
--

CREATE TABLE IF NOT EXISTS `vote` (
  `name` varchar(30) NOT NULL,
  `review` int(11) NOT NULL,
  `npoints` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vote`
--

INSERT INTO `vote` (`name`, `review`, `npoints`, `date`) VALUES
('Agustin Armellini Fischer', 117340, 9, '2014-05-21 12:21:02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
